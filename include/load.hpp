#ifndef LOAD_H
#define LOAD_H

#include <PixelHit.hpp>
#include <MCParticle.hpp>
#include <vector>
#include <string>
#include <map>
#include <TTree.h>

class NodeFile
{
    public:
        NodeFile(std::string _PATH_, std::string _DEBUG_ = "WARNING");
        ~NodeFile();
        std::map<const std::string, TTree*> GetMap();
        std::vector<allpix::PixelHit*>& GetPixelHits();
        std::vector<allpix::MCParticle*>& GetMCParticles();
        unsigned long int GetEvents();
        void SetEntry(unsigned long int);
    private:
        unsigned long int M_ENTRIES;
        TFile* MAIN_FILE;
        std::map<const std::string, TTree*> TREE_MAP;
        TTree* PIXEL_HIT_TREE;
        TBranch* PIXEL_HIT_DBRANCH;
        std::vector<allpix::PixelHit*> PixelHits;
        TTree* MCPARTICLE_TREE;
        TBranch* MCPARTICLE_DBRANCH;
        std::vector<allpix::MCParticle*> MCParticles;
        TTree* MCTRACK_TREE;        
};
#endif