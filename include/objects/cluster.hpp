#include <MCParticle.hpp>
#include <PixelHit.hpp>

class Cluster
{
    public:
        Cluster();
        ~Cluster();
        std::vector<allpix::PixelHit*> GetPixelHitList();
        std::vector<const allpix::MCParticle*> GetMCParticles();
        unsigned long int GetWidth();
        unsigned long int GetHeight();
        unsigned long int GetSize();
        bool IsNormal();
    private:
        std::vector<const allpix::MCParticle*> MCParticlesList;
        std::vector<allpix::PixelHit*> PixHitList;
        void Append(allpix::PixelHit*);
        unsigned int _size_x;
        unsigned int _size_y;
        unsigned int _size_;
        friend class Clusters;
};

class Clusters
{
    public:
        Clusters(std::vector<allpix::PixelHit*> PixelHits, std::vector<allpix::MCParticle*> MCParticles, bool DEBUG);
        ~Clusters();
        unsigned int GetNumber();
        std::vector<Cluster> GetClusterVector();
    private:
        bool _DEBUG_ = 0;
        std::vector<Cluster> ClusterVector;
        bool AreClose(allpix::PixelHit* PIXEL_1, allpix::PixelHit* PIXEL_2);
        void CompleteCluster(Cluster&, std::vector<allpix::PixelHit*>&, unsigned long int);
        unsigned int _size_;
};