import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description = 'Exec Options')
parser.add_argument("--F", type = str)
args = parser.parse_args()

FILE_NAME = args.F

FILE = np.genfromtxt(FILE_NAME, delimiter = ", ")
RIS_EDGE = FILE[0]
FAL_EDGE = FILE[1]
OFFSET = FILE[2]
TOT = FILE[3]

def SquareWave(X, OFFSET):
    Y = np.sin(2 * np.pi * (X - OFFSET) / 25)
    return np.where(Y > 0, 1, 0)

fig, ax = plt.subplots(nrows = 1, ncols = 1)
ax.set_title(r"ToT := {:.0f}".format(TOT))
ax.plot([0, RIS_EDGE, RIS_EDGE, FAL_EDGE, FAL_EDGE, FAL_EDGE + 5 * 25], np.array([0, 0, 1, 1, 0, 0]) + 1.05)
D = np.linspace(start = 0, stop = FAL_EDGE + 5 * 25, num = 1000)
ax.plot(D, SquareWave(D, OFFSET))
plt.show(block = True)

