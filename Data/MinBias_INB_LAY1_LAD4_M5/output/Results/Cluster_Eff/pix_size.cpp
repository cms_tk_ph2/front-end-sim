#define SENSOR_THICKNESS 0.150
#define PITCH_X 0.025
#define PITCH_Y 0.100

#include <PixelHit.hpp>
#include <MCParticle.hpp>
#include <cluster.hpp>
#include <load.hpp>
#include <algorithm>
#include <log.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <iostream>
#include <TMath.h>
#include <string>
#include <vector>
#include <fstream>
#include <TRandom3.h>
#include <TDatabasePDG.h>
#include <boost/program_options.hpp>

namespace po = boost::program_options;
std::string DETECTOR_NAME = "Detector";
TRandom3 RV;
LOGGER _GLOBAL_LOG_;
TDatabasePDG GDatabase;
const std::string STATUS = "INFO";

double TIME_WALK(double _charge, double _threshold) 
{
    double TIME_WALK = (6.24573346e-04 * _threshold + 7.07441732) * exp(-(_charge - _threshold)/(0.0555666 * _threshold + 24.84357041) ) + (16.177236798564252 * _threshold + 2527.537067765581) / _charge + (-0.00038665573601304583 * _threshold);
    // + 14.324674481669573
    return TIME_WALK;
}

void viewer(std::string _PATH_, std::string _GEN_PATH_ , std::string _OUTPUT_NAME_, double _THRESHOLD_ = 1000)
{
    /** PREPARING THE OUTPUT FILE **/
    std::ofstream output;
    output.open(_OUTPUT_NAME_.c_str());
    /*******************************/
    
    /** SETTING UP THE ALLPIX OUTPUT FILE **/
    NodeFile Example(_PATH_, "LOG");
    std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
    std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();
    unsigned long int Events = Example.GetEvents();
    /***************************************/

    /** GENERATOR ROOT FILE **/
    TFile* _main = TFile::Open(_GEN_PATH_.c_str());
    TTree* _tree = (TTree*)_main->FindObjectAny("tree");
    ::_GLOBAL_LOG_.SendInfo("Found Generator ROOT File with " + std::to_string(_tree->GetEntries()) + " Events.");
    /***************************************/

    /** SETTING UP BRANCHES && VARIABLES **/
    Float_t ltof;
    Float_t pt;
    Float_t E;
    _tree->SetBranchAddress("ltof", &ltof);
    _tree->SetBranchAddress("pt", &pt);
    _tree->SetBranchAddress("E", &E);
    /***************************************/

    for (double pt_lower_bound = 0; pt_lower_bound <= 1; pt_lower_bound += 0.1)
    {
         for (double dt = -10; dt < +10; dt += 0.1)
         {
		    TH1D* Cluster_Size_C = new TH1D("c1", "Cluster Size Distribution", 10, 0, 10);
            TH1D* Cluster_Size_N = new TH1D("c2", "Cluster Size Distribution", 10, 0, 10);
            for (unsigned long int _evt_ = 0; _evt_ < Events; _evt_++)
            {
                Example.SetEntry(_evt_); // Sets the PixelHits && MCParticles
                _tree->GetEntry(_evt_);
                if (STATUS == "DEBUG") std::cout << "EVENT #" << _evt_ << std::endl;

                /** APPLYING THRESHOLD **/
                std::vector<allpix::PixelHit*> CUR_PIXELHITS;
                std::vector<allpix::PixelHit*> NXT_PIXELHITS;
                for (auto& pixhit : PixelHits) 
                {
                    if (pixhit->getSignal() > _THRESHOLD_) 
                    {
                        double time = TIME_WALK(pixhit->getSignal(), _THRESHOLD_) + ltof + dt;
                        if (time >= 0 && time < 25) {CUR_PIXELHITS.push_back(pixhit);}
                        else {NXT_PIXELHITS.push_back(pixhit);}
                    }
                }
                /************************/

                // Clusters CLUSTERS(CUR_PIXELHITS, MCParticles, 0);
                if (CUR_PIXELHITS.size() == 0 || pt < pt_lower_bound) {continue;}
                Cluster_Size_C->Fill(CUR_PIXELHITS.size());
                Cluster_Size_N->Fill(NXT_PIXELHITS.size());
           } 
            // std::cout << dt << ", " << pt_lower_bound << ", " << Cluster_Size_C->GetMean() << ", " << Cluster_Size_C->GetStdDev() << ", " << Cluster_Size_N->GetMean() << ", " << Cluster_Size_N->GetStdDev() << std::endl;
        	// output << dt << ", " << pt_lower_bound << ", " << Cluster_Size_C->GetMean() << ", " << Cluster_Size_C->GetStdDev() << ", " << Cluster_Size_N->GetMean() << ", " << Cluster_Size_N->GetStdDev() << std::endl;
            std::vector<double> bins(Cluster_Size_N->GetNcells());
            std::vector<double> centers(Cluster_Size_N->GetNcells());
            for (int bin = 0; bin < Cluster_Size_N->GetNcells(); ++bin) 
            {
                const double binContent = Cluster_Size_N->GetBinContent(bin);
                const double center = Cluster_Size_N->GetBinCenter(bin);;
                bins[bin] += binContent;
                centers[bin] += center;
            }

            output << dt << ", " << pt_lower_bound << ", ";
            for (std::size_t i = 0; i < centers.size(); ++i) 
            {
                if (i == (centers.size() - 1)) {output << centers[i] << ", " << bins[i] << std::endl;}
                else {output << centers[i] << ", " << bins[i] << ", ";}
            }

            for (std::size_t i = 0; i < centers.size(); ++i) {std::cout << dt << std::endl;}
            delete Cluster_Size_C;
            delete Cluster_Size_N;
	    }
     }
        if (STATUS == "DEBUG") std::cout << "*******************************************************************************************************************" << std::endl;
    output.close();
}

int main(int ac, char *av[])
{
    std::string _PATH_;
    std::string _GEN_PATH_;
    std::string _OUTPUT_NAME_;
	double THRESHOLD = 1000;

	po::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("PATH", po::value<std::string>(&_PATH_)->required(), "relative path of .root file")
    ("GEN_PATH", po::value<std::string>(&_GEN_PATH_)->required(), "relative path of generator .root file")
    ("O", po::value<std::string>(&_OUTPUT_NAME_)->required(), "relative path of generator .root file")
	("THR", po::value<double>(&THRESHOLD), "the value of the global chip threshold");

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
        std::cout << desc << "\n";
        return 1;
	}

    if (vm.count("path"))
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << _PATH_ << std::endl;
    }

	_GLOBAL_LOG_.SendInfo("THRESHOLD == " + std::to_string((unsigned long int)THRESHOLD));

    viewer(_PATH_, _GEN_PATH_, _OUTPUT_NAME_, THRESHOLD);
    return 0;
}
