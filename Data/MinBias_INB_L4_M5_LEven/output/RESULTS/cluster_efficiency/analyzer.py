import numpy as np
import matplotlib.pyplot as plt
from tqdm import tqdm

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})
plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

FILE = np.genfromtxt("test.csv", delimiter = ", ")
EVENTS = np.unique(FILE[:,0])

THRESHOLD = 1000

DOMAIN = np.arange(start = -5, stop = +5, step = 0.1)
Y = []

for DELTA_DAQ in tqdm(DOMAIN):
    N_ALL = 0
    N = 0

    for evt in EVENTS:
        if (evt > 5000): break
        INFO = FILE[FILE[:, 0] == evt]
        CLUSTERS = np.unique(INFO[:, 1])
    
        EFF = 0

        for no in CLUSTERS:
            PIXHITS = INFO[INFO[:, 1] == no, :]
            PIXHITS = PIXHITS[0]
            if (PIXHITS[4] > THRESHOLD and PIXHITS[5] + DELTA_DAQ < 25 and PIXHITS[5] + DELTA_DAQ >= 0): EFF = 1
    
        if (EFF == 1):
            N += 1
            N_ALL += 1
        else:
            N_ALL += 1

    Y.append(N / N_ALL)  

fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (8,4))
ax.plot(DOMAIN, Y)
plt.show(block = True)
