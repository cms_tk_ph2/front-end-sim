#include "CROCv1.hpp"
#include <iostream>
#include <TMath.h>

CROCv1::CROCv1(const double _threshold, const double _threshold_dispersion, const double _krum, const double _krum_dispersion, const double _clk_offset, const MODE _mode, const unsigned short int _isolated_hit_removal_max_ToT_, LOGGER& Messenger) 
    : THRESHOLD(_threshold), 
    THRESHOLD_DISPERSION(_threshold_dispersion),
    KRUM(_krum),
    KRUM_DISPERSION(_krum_dispersion),
    CLK_OFFSET(_clk_offset),
    CHIP_MODE(_mode),
    ISOLATED_HIT_MAX_TOT(_isolated_hit_removal_max_ToT_) 
{
    if (!(std::get<0>(CHIP_MODE) == "Latched" || std::get<0>(CHIP_MODE) == "Synched")) 
    {
        Messenger.SendFatal("First Entry allows only Latched and Synched"); std::exit(1);
    }
}

void CROCv1::PrintMeasurement(double _charge, std::string file_name)
{
    std::ofstream output; 
    output.open(file_name.c_str());
    
    CROCv1::CROCv1_CLK CLK(this);
    CROCv1::CROCv1_CMP COMP(this, _charge);

    double COM_RIS_EDGE = COMP.GetRisingEdge();
    double COM_FAL_EDGE = COMP.GetFallingEdge();
    double OFFSET = (this)->CLK_OFFSET;
    unsigned short int ToT = (*this).GetToT(_charge);
    output << COM_RIS_EDGE << ", " << COM_FAL_EDGE << ", " << OFFSET << ", " << ToT << std::endl;
    output.close();
}

void CROCv1::PrintNominalReferenceLadder(std::string file_name)
{
    std::ofstream output; 
    output.open(file_name.c_str());

    const auto RefLadder = (*this).GetNominalReferenceLadderFull();

    for (auto& i : RefLadder)
    {
        output << i.first << ", " << std::get<0>(i.second) << ", " <<  std::get<1>(i.second) << ", " <<  std::get<2>(i.second) << std::endl;
    }
    output.close();
}

void CROCv1::PrintTornadoMap(std::string file_name)
{
    std::ofstream output; 
    output.open(file_name.c_str());

    for (double charge = 0; charge < 4e4; charge += 5)
    {
        for (double fine_delay = -30; fine_delay < 25; fine_delay += 0.1)
        {
            unsigned short int ToT = ((*this).RisingEdge(charge) + fine_delay <= 25 && (*this).RisingEdge(charge) + fine_delay > 0 && charge > THRESHOLD) ? (*this).GetToT(charge) : 15;
            output << charge << ", " << fine_delay << ", " << ToT << std::endl;
        }
    }
    output.close();
}

const double CROCv1::RisingEdge(double _charge) const
{
    /** GENERAL MODEL 
     * A(_threshold) * exp ( - (_charge - _threshold) / B(_threshold) ) + C(_threshold) / _charge
     * @param A, @param B @param C, generally, depend on _threshold. **/
    
    TRandom3 RandomVariable;
    double Applied_Threshold = THRESHOLD + RandomVariable.Gaus(0, THRESHOLD_DISPERSION);

    double A = 7.07; // In nanoseconds.
    double B = 0.06 * Applied_Threshold + 24.8; // In electrons.
    double C = 16.18 * Applied_Threshold + 2527.54; // In electrons * nanoseconds.

    double TimeWalk = A * exp(- (_charge - Applied_Threshold) / (B) ) + C / _charge;
    return TimeWalk;
}

const double CROCv1::FallingEdge(double _charge) const
{
    /** GENERAL MODEL 
     * A(_threshold) * exp ( - (_charge - _threshold) / B(_threshold) ) + C(_threshold) / _charge
     * @param A, @param B, generally, depend on _threshold and _krum. **/

    TRandom3 RandomVariable;
    double Applied_Krum = KRUM + RandomVariable.Gaus(0, KRUM_DISPERSION);
    double Applied_Threshold = THRESHOLD + RandomVariable.Gaus(0, THRESHOLD_DISPERSION);
    
    double A = 1.28 / (0.84 * Applied_Krum + 0.92);
    double B = - 24.13 * (Applied_Krum - 0.05 * (Applied_Threshold - 988.64));
    return A * (_charge - B);
}

const double CROCv1::TimeDuration(double _charge) const {return (FallingEdge(_charge) - RisingEdge(_charge));}

const unsigned short int CROCv1::GetToT(double _charge)
{
    unsigned short int ToT = -1;
    CROCv1::CROCv1_CLK CLK(this);
    CROCv1::CROCv1_CMP COMP(this, _charge);
    double COM_RIS_EDGE = COMP.GetRisingEdge();
    double COM_FAL_EDGE = COMP.GetFallingEdge();
    const std::vector<double> CLK_RIS_EDGES = CLK.CountRisingEdges({COM_RIS_EDGE, COM_FAL_EDGE});
    ToT = CLK_RIS_EDGES.size() - 1; // ToT starts the count from 0.
    return (ToT > 14 ? 14 : ToT);
}

const unsigned short int CROCv1::GetToT(const allpix::PixelHit* PixHit, const std::vector<allpix::PixelHit*> RefPixHits) const
{
    unsigned short int ToT = -1;
    CROCv1::CROCv1_CLK CLK(this);
    double _charge = PixHit->getSignal();
    CROCv1::CROCv1_CMP COMP(this, _charge);
    return 0;
}

const std::map<unsigned short int, double> CROCv1::GetNominalReferenceLadder()
{
    std::map<unsigned short int, std::vector<double>> ToT2ChargeInterval;
    std::map<unsigned short int, std::pair<double, double>> ToT2Widths;
    std::map<unsigned short int, double> Output;

    for (double charge = THRESHOLD; charge < 2e4; charge += 1)
    {
        unsigned short int ToT = (*this).GetToT(charge);
        if (ToT < 14) ToT2ChargeInterval[ToT].push_back(charge);
    }

    for (const auto& ToT : ToT2ChargeInterval)
    {
        if (ToT.first < 14)
        {
            std::vector<double> Charges = ToT.second;
            auto pair = std::minmax_element(Charges.begin(), Charges.end());
            double MIN = *pair.first;
            double MAX = *pair.second;
            ToT2Widths[ToT.first] = std::make_pair(MIN, MAX);
        }
    }

    std::function<double (double)> Nominal_Landau = [&](double _charge){return TMath::Landau(_charge, 1.01708e+04, 9.80497e+02, true);};
    for (const auto& ToT : ToT2Widths)
    {
        unsigned short int ToT_Value = ToT.first;
        if (ToT_Value < 14)
        {
            double MIN = ToT.second.first;
            double MAX = ToT.second.second;
            double MEDIAN = (MIN + MAX) / 2.0;
            Output[ToT_Value] = MEDIAN;
        }
        else
        {
            double charge_estimate = 0;
            for (double charge = ToT.second.first; charge < 1e5; charge += 1)
            {
                charge_estimate += charge * Nominal_Landau(charge);
            }
            Output[ToT_Value] = charge_estimate;
        }
    }

    return Output;
}

const std::map<unsigned short int, std::tuple<double, double, double>> CROCv1::GetNominalReferenceLadderFull()
{
    std::map<unsigned short int, std::vector<double>> ToT2ChargeInterval;
    std::map<unsigned short int, std::pair<double, double>> ToT2Widths;
    std::map<unsigned short int, std::tuple<double, double, double>> Output;

    for (double charge = THRESHOLD; charge < 7e4; charge += 1)
    {
        unsigned short int ToT = (*this).GetToT(charge);
        ToT2ChargeInterval[ToT].push_back(charge);
    }

    for (const auto& ToT : ToT2ChargeInterval)
    {
        std::vector<double> Charges = ToT.second;
        auto pair = std::minmax_element(Charges.begin(), Charges.end());
        double MIN = *pair.first;
        double MAX = *pair.second;
        ToT2Widths[ToT.first] = std::make_pair(MIN, MAX);
    }

    // std::function<double (double)> Nominal_Landau = [](double _charge)
    // {
    //     TF1 Nominal_Landau("NL", "x * TMath::Landau(x, 1.01708e+04, 9.80497e+02)", 0, 1e5);
    //     // return TMath::Landau(_charge, 1.01708e+04, 9.80497e+02, true);
    //     return Nominal_Landau;
    // };
    
    for (const auto& ToT : ToT2Widths)
    {
        unsigned short int ToT_Value = ToT.first;
        if (ToT_Value < 14)
        {
            double MIN = ToT.second.first;
            double MAX = ToT.second.second;
            double MEDIAN = (MIN + MAX) / 2.0;
            std::get<0>(Output[ToT_Value]) = MIN;
            std::get<1>(Output[ToT_Value]) = MAX;
            std::get<2>(Output[ToT_Value]) = MEDIAN;
        }
        else
        {
            double MIN = ToT.second.first;
            double MAX = ToT.second.second;
            TF1 Expected_Nominal_Landau("NL", "x * TMath::Landau(x, 1.01708e+04, 9.80497e+02)", MIN, 1e5);
            TF1 Nominal_Landau("NL", "TMath::Landau(x, 1.01708e+04, 9.80497e+02)", MIN, 1e5);
            double charge_estimate = Expected_Nominal_Landau.Integral(MIN, 1e5) / Nominal_Landau.Integral(MIN, 1e5);

            std::get<0>(Output[ToT_Value]) = MIN;
            std::get<1>(Output[ToT_Value]) = MAX;
            std::get<2>(Output[ToT_Value]) = charge_estimate;
        }
    }
    return Output;
}