#include "CROCv1.hpp"
#define PI 3.14159265

CROCv1::CROCv1_CLK::CROCv1_CLK(const CROCv1* PARENT)
{
    const double CLK_OFFSET = PARENT->CLK_OFFSET;
    OFFSET = CLK_OFFSET;
}

const bool CROCv1::CROCv1_CLK::GetSignal(double TimeRef) const
{
    /** For TimeRef = 25.0 * k + OFFSET, k in Z, TimeRef is a Rising Edge
     *  For TimeRef = 25.0 * k + 25.0 + OFFSET / 2.0 = 25.0 (1/2 + k) + OFFSET, TimeRef is a Rising Edge **/
    return (bool)(floor(sin(2 * PI * (TimeRef - OFFSET) / 25.0)) + 1);
}

const std::vector<double> CROCv1::CROCv1_CLK::CountRisingEdges(const std::pair<double, double>& INTERVAL) const
{
    std::vector<double> Rising_Edges;
    const double MIN = INTERVAL.first;
    const double MAX = INTERVAL.second;
    const signed short int K_MAX = floor((MAX - OFFSET) / 25.0);
    const signed short int K_MIN = ceil((MIN - OFFSET) / 25.0);
    signed short int ToT = -1;
    const double TIME_MAX = GetKthRisingEdge(K_MAX);
    const double TIME_MIN = GetKthRisingEdge(K_MIN);

    if (TIME_MAX < TIME_MIN) {return Rising_Edges;}
    else 
    {
        for (unsigned short int k = K_MIN; k <= K_MAX; k++) {Rising_Edges.push_back(GetKthRisingEdge(k));}
    }
    return Rising_Edges;      
}

const double CROCv1::CROCv1_CLK::GetKthRisingEdge(const signed short int K) const {return (25.0 * K + OFFSET);}
const double CROCv1::CROCv1_CLK::GetKthFallingEdge(const signed short int K) const {return (25.0 * (1/2 + K) + OFFSET);}

CROCv1::CROCv1_CMP::CROCv1_CMP(const CROCv1* PARENT, const double _charge)
{
    RIS_EDGE = PARENT->RisingEdge(_charge);
    FAL_EDGE = PARENT->FallingEdge(_charge);
}

const double CROCv1::CROCv1_CMP::GetRisingEdge() const {return RIS_EDGE;}
const double CROCv1::CROCv1_CMP::GetFallingEdge() const {return FAL_EDGE;}
const bool CROCv1::CROCv1_CMP::GetSignal(const double TimeRef) const {return (TimeRef <= FAL_EDGE && TimeRef >= RIS_EDGE);}