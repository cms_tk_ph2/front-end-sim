import numpy as np
import matplotlib.pyplot as plt

FILE = np.genfromtxt("output.csv", delimiter = ", ")

plt.plot(FILE[:, 0], FILE[:, 2] / FILE[:, 3])
plt.show(block = True)
