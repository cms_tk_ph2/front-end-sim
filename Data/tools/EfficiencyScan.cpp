#include <PixelHit.hpp>
#include <MCParticle.hpp>
#include <cluster.hpp>
#include <load.hpp>
#include <algorithm>
#include <log.hpp>
#include <CROCv1.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <iostream>
#include <TMath.h>
#include <string>
#include <vector>
#include <fstream>
#include <TRandom3.h>
#include <TDatabasePDG.h>

#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/foreach.hpp>

namespace po = boost::program_options;
std::string DETECTOR_NAME = "Detector";
TRandom3 RV;
LOGGER _GLOBAL_LOG_;
TDatabasePDG GDatabase;

namespace POptions  = boost::program_options;
namespace PTree		= boost::property_tree;

/* EXECUTABLE OPTIONS */
std::string ALLPIX_ROOT_PATH;
std::string ALLPIX_GENERATOR_PATH;
std::string OUTPUT_NAME;
std::string STATUS = "INFO";
double THRESHOLD = 1000;
double THRESHOLD_DISPERSION = 100;
double ELECTRONIC_NOISE = 10;
double PHASE_MIN = -10;
double PHASE_MAX = +10;
double PHASE_STEP = 0.1;
double EFFICIENCY_LOW = 0.95;

void EvaluateEfficiency(const double& _time, bool& eff_var, unsigned long int _triggers = 1) {if (_time >= 0 && _time < 25 * _triggers && eff_var == 0) {eff_var = 1;}}

void viewer()
{
    CROCv1 Readout_Chip(THRESHOLD, THRESHOLD_DISPERSION, 70, 0, 0, {"Synched", "Double", "Double"}, 0, _GLOBAL_LOG_);

    /** PREPARING THE OUTPUT FILE **/
    std::ofstream output;
    output.open(OUTPUT_NAME.c_str());
    /*******************************/
    
    /** SETTING UP THE ALLPIX OUTPUT FILE **/
    NodeFile Example(ALLPIX_ROOT_PATH, "LOG");
    std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
    std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();
    unsigned long int Events = Example.GetEvents();
    /***************************************/

    /** GENERATOR ROOT FILE **/
    TFile* _main = TFile::Open(ALLPIX_GENERATOR_PATH.c_str());
    TTree* _tree = (TTree*)_main->FindObjectAny("tree");
    ::_GLOBAL_LOG_.SendInfo("Found Generator ROOT File with " + std::to_string(_tree->GetEntries()) + " Events.");
    /***************************************/

    /** SETTING UP BRANCHES && VARIABLES **/
    Float_t ltof, pt, E;

    _tree->SetBranchAddress("ltof", &ltof);
    _tree->SetBranchAddress("pt", &pt);
    _tree->SetBranchAddress("E", &E);
    /***************************************/

    for (double dt = PHASE_MIN; dt <= +PHASE_MAX; dt += PHASE_STEP)
    {
        unsigned long int N_eff = 0;
        unsigned long int N_eff_ltof = 0;
        unsigned long int N_eff_timewalk = 0;
        unsigned long int N_eff_timewalk_ltof = 0;

        unsigned long int N_all = 0;

        for (unsigned long int _evt_ = 0; _evt_ < Events; _evt_++)
        {
            Example.SetEntry(_evt_); // Sets the PixelHits && MCParticles
            _tree->GetEntry(_evt_);  // Sets the CMSSW Event Features
            if (STATUS == "DEBUG") std::cout << "EVENT #" << _evt_ << std::endl;

            /** APPLYING THRESHOLD **/
            std::vector<allpix::PixelHit*> MOD_PIXELHITS;
            for (auto& pixhit : PixelHits) {if (pixhit->getSignal() > THRESHOLD) MOD_PIXELHITS.push_back(pixhit);}
            /************************/

            Clusters CLUSTERS(MOD_PIXELHITS, MCParticles, 0); 
            if (MOD_PIXELHITS.size() == 0) {continue;} /* !We explicitly ignore events that do not register anything on the sensor. */

            bool is_eff = 0;
            bool is_eff_ltof = 0;
            bool is_eff_timewalk = 0;
            bool is_eff_ltof_timewalk = 0;

            for (unsigned long int index = 0; index < CLUSTERS.GetClusterVector().size(); index++)
            {
                auto C = CLUSTERS.GetClusterVector()[index];

                if (STATUS == "DEBUG") std::cout << "  Cluster #" << index + 1 << " [Size: " << C.GetSize() << ", Height: " << C.GetHeight() << ", Width: " << C.GetWidth() << "]:" << std::endl;
                for (auto& pixhit : C.GetPixelHitList())
                {
                    /** All timings defined below are in >> nanoseconds.
                     * @param T_ALL  := ltof + RD53B-CMS TimeWalk + Clock_Phase + Signal Distribution Fluctuations.
                     * @param T_LTOF := timing with only ltof into consideration (equivalent with a perfect, infinitely fast AFE).
                     * @param T_TIMEWALK := timing with only timewalk into consideration (equivalent with all hits coming in-phase with the AFE CLK).
                     * @param T_LTOF_TIMEWALK := timing with only timewalk + ltof into consideration (equivalent with ideal/dispersionless singal distribution).
                     **/

                    double _timewalk = Readout_Chip.RisingEdge(pixhit->getSignal());
                    double _clkdist = (RV.Rndm());
                    double          T_LTOF =    dt + ltof;
                    double      T_TIMEWALK =    dt + _timewalk;
                    double T_LTOF_TIMEWALK =    dt + _timewalk + ltof;
                    double           T_ALL =    dt + _timewalk + ltof + _clkdist;

                    /** Evaluating Efficiencies ... **/
                    
                    EvaluateEfficiency(T_ALL, is_eff, 1);
                    EvaluateEfficiency(T_TIMEWALK, is_eff_timewalk, 1);
                    EvaluateEfficiency(T_LTOF, is_eff_ltof, 1);
                    EvaluateEfficiency(T_LTOF_TIMEWALK, is_eff_ltof_timewalk, 1);

                    /** Printing for Debugging **/

                    bool pix_eff = (T_ALL >= 0 && T_ALL < 25) ? 1 : 0;
                    std::string BX = (pix_eff) ? "\033[1;32m" + std::to_string(T_ALL) + "\033[0;37m" : "\033[1;31m" + std::to_string(T_ALL) + "\033[0;37m";
                    if (STATUS == "DEBUG") std::cout << "      > LOC: (" << pixhit->getPixel().getLocalCenter().x() << ", " << pixhit->getPixel().getLocalCenter().y() << ") mm, SIGNAL: " << pixhit->getSignal() << "e, TIMIMG (CLK Phase + ltof + Timewalk + CLK Dist): " << ltof << " + " << _timewalk << " + " << dt << " + " << _clkdist << " = " << BX << " ns" << std::endl;
                    
                    // output << _evt_ << ", " << index + 1 << ", " << pixhit->getPixel().getLocalCenter().x() << ", " << pixhit->getPixel().getLocalCenter().y() << ", " << pixhit->getSignal() << ", " << TIME_WALK(pixhit->getSignal(), _THRESHOLD_) + ltof << ", " << pt << std::endl;
                }
                bool One = (C.GetMCParticles().size() == 1) ? 1 : 0;
                if (One)
                {
                    auto MCP = C.GetMCParticles()[0];
                    auto Name = (GDatabase.GetParticle(MCP->getParticleID()) != nullptr) ? (GDatabase.GetParticle(MCP->getParticleID())->GetName()) : "Empty";
                    if (STATUS == "DEBUG") std::cout << "  by MCParticle @" << MCP << ": " << std::endl;
                    if (STATUS == "DEBUG") std::cout << "      > LOC: (" << MCP->getLocalStartPoint().x() << ", " << MCP->getLocalStartPoint().y() << ")" << ", PID: " << MCP->getParticleID() << " (" << Name << "), pT: " << pt << " GeV, E: " << E << " GeV" << std::endl;
                }
                else
                {
                    if (STATUS == "DEBUG") std::cout << "  by MCParticles @" << (&C.GetMCParticles()[0]) << ": " << std::endl;
                    for (auto& MCP : C.GetMCParticles())
                    {
                        auto Name = (GDatabase.GetParticle(MCP->getParticleID()) != nullptr) ? (GDatabase.GetParticle(MCP->getParticleID())->GetName()) : "Empty";
                        if (STATUS == "DEBUG") std::cout << "      > LOC: (" << MCP->getLocalStartPoint().x() << ", " << MCP->getLocalStartPoint().y() << ")" << ", PID: " << MCP->getParticleID() << " (" << Name << "), E: " << " " << std::endl;
                    }
                }
            }
            if (STATUS == "DEBUG") std::cout << "EVENT #" << _evt_ << ", EFFICIENT EVENT? " << is_eff << std::endl;
            
            N_eff += is_eff;
            N_eff_ltof += is_eff_ltof;
            N_eff_timewalk += is_eff_timewalk;
            N_eff_timewalk_ltof += is_eff_ltof_timewalk;
            N_all += 1;
        }

        double Run_Efficiency_All = (double)N_eff / N_all;
        _GLOBAL_LOG_.SendInfo("Efficiency of Run: " + (std::string)_GREEN_ + std::to_string(Run_Efficiency_All) + _RETURN_ + " @ " + std::to_string(dt) + " ns, @ " + std::to_string(0) + " GeV/c Cut");
        output << dt << ", " << N_eff_ltof << ", " << N_eff_timewalk << ", " << N_eff_timewalk_ltof << ", " << N_eff << ", " << N_all << std::endl;
    }
    if (STATUS == "DEBUG") std::cout << "*******************************************************************************************************************" << std::endl;
    output.close();
}

int main(int ac, char *av[])
{
    std::string CONFIG_PATH;

	POptions::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("c", POptions::value<std::string>(&CONFIG_PATH)->required(), "relative path of the config file");

	POptions::variables_map vm;
	POptions::store(POptions::parse_command_line(ac, av, desc), vm);
	POptions::notify(vm);

	if (vm.count("help")) 
    {
        std::cout << desc << "\n";
        return 1;
	}

    if (vm.count("c"))
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << CONFIG_PATH << std::endl;
    }

    PTree::ptree pt;
	PTree::ini_parser::read_ini(CONFIG_PATH, pt);
    if (pt.count("SIMULATE"))
    {
        std::vector<std::string> EXPECTED_CHILDREN{"THRESHOLD", "THRESHOLD_DISPERSION", "ALLPIX_ROOT_FILE", 
                    "GENERATOR_ROOT_FILE", "OUTPUT_NAME", "PHASE_START", "PHASE_STOP", "PHASE_STEP", "ELECTRONIC_NOISE", "LOG_LEVEL", "EFFICIENCY_LOW"};
        
        BOOST_FOREACH(const PTree::ptree::value_type &v, pt.get_child("SIMULATE"))
        {
            /** VARIABLES 
             * @param KEY_NAME  := The name of the variable the user wishes to set.
             * @param VALUE		:= The value assigned to that @param KEY_NAME.
             **/
            std::string KEY_NAME = v.first.data();
            std::string VALUE = v.second.data();
            
            /** Check if the KEY_NAME exists in the expected list of expected key names **/
            std::vector<std::string>::iterator finder = std::find(EXPECTED_CHILDREN.begin(), EXPECTED_CHILDREN.end(), KEY_NAME);
            size_t index = finder - EXPECTED_CHILDREN.begin();
            
            /** For index < 3, the key_name is !mandatory!. Otherwise, not. **/
            if (finder != EXPECTED_CHILDREN.end())
            {
                _GLOBAL_LOG_.SendInfo("Required child \"" + KEY_NAME + "\" was found with a value of \"" + VALUE + "\".");
                if (KEY_NAME == "THRESHOLD") {THRESHOLD = std::stod(VALUE);}
                else if (KEY_NAME == "THRESHOLD_DISPERSION") {THRESHOLD_DISPERSION = std::stod(VALUE);}
                else if (KEY_NAME == "ALLPIX_ROOT_FILE") {ALLPIX_ROOT_PATH = VALUE;}
                else if (KEY_NAME == "GENERATOR_ROOT_FILE") {ALLPIX_GENERATOR_PATH = VALUE;}
                else if (KEY_NAME == "OUTPUT_NAME") {OUTPUT_NAME = VALUE;}
                else if (KEY_NAME == "LOG_LEVEL") {STATUS = VALUE;}
                else if (KEY_NAME == "EFFICIENCY_LOW") {EFFICIENCY_LOW = std::stod(VALUE);}
            }
            else if (finder == EXPECTED_CHILDREN.end())
            {
                _GLOBAL_LOG_.SendFatal("Required child \"" + KEY_NAME + "\" was not found. Aborting (Exit Code = 1)");
                /** If a required key is not set, the application is terminated with exit code 1 **/
                std::exit(1);
            }
        }
    }
    viewer();
    return 0;
}