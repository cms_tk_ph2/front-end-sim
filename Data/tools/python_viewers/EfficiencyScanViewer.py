## This script plots efficiency scans for any abstract set of data. 
## The input file needs to be in the form:
## [dt], [ltof_eff], [Timewalk_eff], [Timewalk + ltof _eff], [eff]

import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

FILE = np.genfromtxt("example.csv", delimiter = ", ")

fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (5,3))

ax.plot(-FILE[:, 0], FILE[:, 3] / FILE[:, 5], label = r"$\varepsilon$ (Timewalk + ltof)", ls = '--', color = 'green')
ax.plot(-FILE[:, 0], FILE[:, 1] / FILE[:, 5], label = r"$\varepsilon$ (ltof)", ls = '--', color = 'orange')
ax.plot(-FILE[:, 0], FILE[:, 2] / FILE[:, 5], label = r"$\varepsilon$ (Timewalk)", ls = '--', color = 'red')
ax.plot(-FILE[:, 0], FILE[:, 4] / FILE[:, 5], label = r"$\varepsilon$ (All Effects)", ls = '-', lw = 2.0, color = 'black')
ax.axhline(max(FILE[:, 4] / FILE[:, 5]), color = 'blue', ls = '-', lw = 2.0, alpha = 0.5, label = r"Maximum $\varepsilon := $ {:.0f}\%".format(100 * max(FILE[:, 4] / FILE[:, 5])))

ax.set_ylabel(r"Efficiency", fontsize = 15)
ax.set_xlabel(r"DAQ Clock Phase", fontsize = 15) 
ax.set_ylim([0.95, 1.005])
ax.set_xlim([-2.5, 5])
ax.legend(fontsize = 10)
fig.savefig("LAY4LaddEven.png", dpi = 500, bbox_inches = "tight")
plt.show(block = True)
