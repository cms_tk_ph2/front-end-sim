import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

FILE = np.genfromtxt("output.csv", delimiter = ", ")

fig, ax = plt.subplots(nrows = 1, ncols = 1)

ax.plot(FILE[:, 0], FILE[:,1] / 160901, color = 'blue', ls = '--', lw = 2.0, label = r"Previous BX")
ax.plot(FILE[:, 0], FILE[:,2] / 160901, color = 'orange', ls = '--', lw = 2.0, label = r"Current BX")
ax.plot(FILE[:, 0], FILE[:,3] / 160901, color = 'red', ls = '--', lw = 2.0, label = r"Next BX")

ax.set_ylabel(r"\# of PixHits", fontsize = 15)
ax.set_xlabel(r"$\Delta t_{DAQ}$ [ns]", fontsize = 15)
ax.legend(fontsize = 15, loc = 'lower right')

plt.show(block = True)
