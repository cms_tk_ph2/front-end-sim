import numpy as np
import matplotlib.pyplot as plt
import argparse

parser = argparse.ArgumentParser(description = 'Graph Options')

args = parser.parse_args()


FILE = np.genfromtxt("res_scan.csv", delimiter = ", ")
FILE1 = np.genfromtxt("res_scan_bfield.csv", delimiter = ", ")
ALPHAS = np.unique(FILE[:,0])
FILE = FILE[FILE[:, 1].argsort()]

fig, ax = plt.subplots(nrows = 1, ncols = 1)
for alpha in ALPHAS:
    color = np.random.rand(3,)
    BETA_RANGE = FILE[FILE[:,0] == alpha, 1]
    BETA_RANGE1 = FILE1[FILE1[:,0] == alpha, 1]
    MEAN = FILE[FILE[:,0] == alpha, 3]
    MEAN1 = FILE1[FILE1[:,0] == alpha, 3]
    ax.scatter(BETA_RANGE, MEAN, label = r"{}".format(alpha), marker = 's', s = 50., edgecolor = 'black', facecolor = np.random.rand(3,))
    ax.scatter(BETA_RANGE1, MEAN1, label = r"{}".format(alpha), marker = 's', s = 50., edgecolor = 'black', facecolor = np.random.rand(3,))
    #ax.plot(BETA_RANGE, MEAN * 1e3, label = r"{}".format(alpha))

for i in range(1, 8):
    ax.axvline(np.rad2deg(np.arctan(i * 100/150.)), color = 'black', linestyle = '--')

plt.show(block = True)
