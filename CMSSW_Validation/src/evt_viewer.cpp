#define SENSOR_THICKNESS 0.150
#define PITCH_X 0.025
#define PITCH_Y 0.100
#define SIZE_CUT 4

#include <PixelHit.hpp>
#include <MCParticle.hpp>
#include <cluster.hpp>
#include <load.hpp>
#include <log.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <iostream>
#include <TMath.h>
#include <string>
#include <vector>
#include <fstream>
#include <boost/program_options.hpp>

namespace po = boost::program_options;
std::string DETECTOR_NAME = "Detector";
LOGGER LOG;

std::vector<unsigned long int> ReturnMinima(std::vector<double>& _INPUT_VEC_)
{
        const std::vector<double>::iterator START = _INPUT_VEC_.begin();
        const std::vector<double>::iterator END   = _INPUT_VEC_.end();
        unsigned long int min = std::min_element(START, END) - START;
        std::vector<unsigned long int> Min_Addresses {min};
        for (unsigned long int index = 0; index < _INPUT_VEC_.size(); ++index) if (_INPUT_VEC_[index] == _INPUT_VEC_[min] && index != min) {Min_Addresses.push_back(index);}
        return Min_Addresses;
}

std::vector<unsigned long int> ReturnMaxima(std::vector<double>& _INPUT_VEC_)
{
        const std::vector<double>::iterator START = _INPUT_VEC_.begin();
        const std::vector<double>::iterator END   = _INPUT_VEC_.end();
        unsigned long int max = std::max_element(START, END) - START;
        std::vector<unsigned long int> Max_Addresses {max};
        for (unsigned long int index = 0; index < _INPUT_VEC_.size(); ++index) if (_INPUT_VEC_[index] == _INPUT_VEC_[max] && index != max) {Max_Addresses.push_back(index);}
        return Max_Addresses;
}

std::tuple<double, double, double> GetReconstructedHit(Cluster& CLUSTER, const double THRESHOLD, const double SIZE_CUT_X, const double SIZE_CUT_Y, 
				const double EFF_LOW_CHARGE_CUT_X, const double EFF_HIGH_CHARGE_CUT_X, const double EFF_LOW_CHARGE_CUT_Y, const double EFF_HIGH_CHARGE_CUT_Y)
{
    if (!CLUSTER.IsNormal()) {return {0, 0, 0};}
    else
    {
        auto PMCParticle = CLUSTER.GetMCParticles()[0];
        auto PixHits = CLUSTER.GetPixelHitList();
        double _alpha_, _beta_;
        _alpha_ = TMath::RadToDeg() * TMath::ATan( (PMCParticle->getLocalEndPoint().x() - PMCParticle->getLocalStartPoint().x()) / (PMCParticle->getLocalEndPoint().z() - PMCParticle->getLocalStartPoint().z()) );
        _beta_ = TMath::RadToDeg() * TMath::ATan( (PMCParticle->getLocalEndPoint().y() - PMCParticle->getLocalStartPoint().y()) / (PMCParticle->getLocalEndPoint().z() - PMCParticle->getLocalStartPoint().z()) );
        double XR, YR, ZR;

        std::vector<double> X;
        std::vector<double> Y;
        std::vector<double> Z;
        std::vector<double> Charge;
            
        for (auto& pixhit : PixHits)
        {
            double Signal = pixhit->getSignal();
            if (Signal > THRESHOLD)
            {
                X.push_back(pixhit->getPixel().getLocalCenter().x());
                Y.push_back(pixhit->getPixel().getLocalCenter().y());
                Z.push_back(pixhit->getPixel().getLocalCenter().z());
                Charge.push_back(pixhit->getSignal());
            }
        }

        if (!Charge.size()) {std::exit(1);}

        std::vector<unsigned long int> min_indices_x = ::ReturnMinima(X);
        std::vector<unsigned long int> min_indices_y = ::ReturnMinima(Y);
        std::vector<unsigned long int> max_indices_x = ::ReturnMaxima(X);
        std::vector<unsigned long int> max_indices_y = ::ReturnMaxima(Y);

        double XG = (*std::min_element(X.begin(), X.end()) + *std::max_element(X.begin(), X.end())) / 2.;
        double YG = (*std::min_element(Y.begin(), Y.end()) + *std::max_element(Y.begin(), Y.end())) / 2.;
        double ZG = (*std::min_element(Z.begin(), Z.end()) + *std::max_element(Z.begin(), Z.end())) / 2.;

        double charge_row_min = 0;
        double charge_row_max = 0;
        double charge_col_min = 0;
        double charge_col_max = 0;

        unsigned long int size_x = CLUSTER.GetWidth();
        unsigned long int size_y = CLUSTER.GetHeight();
        
        for (auto& index : min_indices_x) charge_row_min += Charge[index];
        for (auto& index : max_indices_x) charge_row_max += Charge[index];
        for (auto& index : min_indices_y) charge_col_min += Charge[index];
        for (auto& index : max_indices_y) charge_col_max += Charge[index];

        /* Calculating Terms that Contribute to the Reconstruction on the X-Direction */

        double multip_term_x = ((charge_row_max - charge_row_min) / (2 * (charge_row_min + charge_row_max)));
        double W_Inner_x = (size_x > 2) ? (size_x - 2) * PITCH_X : 0;
        double default_path_x = TMath::Abs(0.150 * TMath::Tan(TMath::DegToRad() * _alpha_));
        double w_eff_x = std::abs(std::abs(default_path_x) - W_Inner_x);
        double abs_value_x = ((size_x >= SIZE_CUT_X) || (w_eff_x / PITCH_X < EFF_LOW_CHARGE_CUT_X | w_eff_x / PITCH_X > EFF_HIGH_CHARGE_CUT_X)) ? PITCH_X : w_eff_x;

        /* Calculating Terms that Contribute to the Reconstruction on the Y-Direction */

        double multip_term_y = ((charge_col_max - charge_col_min) / (2 * (charge_col_min + charge_col_max)));
        double W_Inner_y = (size_y > 2) ? (size_y - 2) * PITCH_Y : 0;
        double default_path_y = TMath::Abs(0.150 * TMath::Tan(TMath::DegToRad() * _beta_));
        double w_eff_y = std::abs(std::abs(default_path_y) - W_Inner_y);
        double abs_value_y = ((size_y >= SIZE_CUT_Y) || (w_eff_y / PITCH_Y < EFF_LOW_CHARGE_CUT_Y | w_eff_y / PITCH_Y > EFF_HIGH_CHARGE_CUT_Y)) ? PITCH_Y : w_eff_y;

        /* Setting the Reconstructed Position in the Three Directions */

        XR = XG + multip_term_x * abs_value_x;
        YR = YG + multip_term_y * abs_value_y;
        ZR = ZG;

        return std::tuple<double, double, double>({XR, YR, ZR});
    }
    return {0,0,0};
}

void viewer(const std::string _PATH_, unsigned long int _evt_, const double THRESHOLD, const double SIZE_CUT_X, const double SIZE_CUT_Y,
                                const double EFF_LOW_CHARGE_CUT_X, const double EFF_HIGH_CHARGE_CUT_X, const double EFF_LOW_CHARGE_CUT_Y, const double EFF_HIGH_CHARGE_CUT_Y)
{
    NodeFile Example(_PATH_, "LOG");
    std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
    std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();
    std::ofstream pixhits, mcparticles, recon;
    pixhits.open("./.event.csv"); mcparticles.open("./.mcps.csv"); recon.open("./.rec.csv");
    Example.SetEntry(_evt_);
    Clusters __C__(PixelHits, MCParticles, 0);

    if (__C__.GetNumber() == 0) {LOG.SendSuccess("Found No Clusters in the Event"); std::exit(1);}
    else if (__C__.GetNumber() == 1) {LOG.SendSuccess("Found 1 Cluster.");}
    else if (__C__.GetNumber() > 1) {LOG.SendSuccess("Found " + std::to_string(__C__.GetNumber()) + " Clusters.");}

    for (unsigned long int cluster_number = 0; cluster_number < __C__.GetClusterVector().size(); cluster_number++)
    {
        auto cluster = __C__.GetClusterVector()[cluster_number];
        auto residual = GetReconstructedHit(cluster, THRESHOLD, SIZE_CUT_X, SIZE_CUT_Y,
                                EFF_LOW_CHARGE_CUT_X, EFF_HIGH_CHARGE_CUT_X, EFF_LOW_CHARGE_CUT_Y, EFF_HIGH_CHARGE_CUT_Y);
        auto pixhitlist = cluster.GetPixelHitList();
        auto mcplist = cluster.GetMCParticles();

        if (!cluster.IsNormal()) {std::cout << "Not a Normal Event" << std::endl; std::exit(1);}
        else
        {
            for (auto& pixhit : pixhitlist)
            {
                pixhits << cluster_number + 1 << ", " << pixhit->getPixel().getLocalCenter().x() << ", " << pixhit->getPixel().getLocalCenter().y() << ", " << pixhit->getPixel().getLocalCenter().z() << ", " <<  pixhit->getSignal() << std::endl;
            }
        
            for (auto& mcparticle : mcplist)
            {
                mcparticles << cluster_number + 1 << ", " << mcparticle->getLocalStartPoint().x() << ", " << mcparticle->getLocalStartPoint().y() << ", " << mcparticle->getLocalStartPoint().z() << ", " << mcparticle->getLocalEndPoint().x() << ", " << mcparticle->getLocalEndPoint().y() << ", " << mcparticle->getLocalEndPoint().z() << std::endl;
            }

            double XR = std::get<0>(residual); double YR = std::get<1>(residual); double ZR = std::get<2>(residual);
            recon << cluster_number + 1 << ", " << XR << ", " << YR << ", " << ZR << std::endl;
            LOG.SendSuccess("Wrote Reconstructed Position: (" + std::to_string(XR) + ", " + std::to_string(YR) + ", " + std::to_string(ZR) + ")");
            
        }
    }
    pixhits.close();
    mcparticles.close();
    recon.close();
}

int main(int ac, char *av[])
{
    std::string _PATH_;
    unsigned long int _EVENT_;
    double THRESHOLD = 1000;
	double SIZE_CUT_X = 4;
	double SIZE_CUT_Y = 4;
	double EFF_LOW_CHARGE_CUT_X = 3;
	double EFF_HIGH_CHARGE_CUT_X = 0;
	double EFF_LOW_CHARGE_CUT_Y = 3;
	double EFF_HIGH_CHARGE_CUT_Y = 0;

	po::options_description desc("Allowed options");
	desc.add_options()
    	("help", "produce help message")
        ("path", po::value<std::string>(&_PATH_)->required(), "relative path of .root file")
        ("THR", po::value<double>(&THRESHOLD), "the value of the global chip threshold")
        ("SIZE_CUT_X", po::value<double>(&SIZE_CUT_X), "the value of the size cut on x")
        ("SIZE_CUT_Y", po::value<double>(&SIZE_CUT_Y), "the value of the size cut on y")
        ("EFF_LOW_CHARGE_CUT_X", po::value<double>(&EFF_LOW_CHARGE_CUT_X), "the value of the size cut on x")
        ("EFF_HIGH_CHARGE_CUT_X", po::value<double>(&EFF_HIGH_CHARGE_CUT_X), "the value of the size cut on x")
        ("EFF_LOW_CHARGE_CUT_Y", po::value<double>(&EFF_LOW_CHARGE_CUT_Y), "the value of the size cut on y")
        ("EFF_HIGH_CHARGE_CUT_Y", po::value<double>(&EFF_HIGH_CHARGE_CUT_Y), "the value of the size cut on y")
    	("event", po::value<unsigned long int>(&_EVENT_), "set event");

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
    		std::cout << desc << "\n";
    		return 1;
	}

    if (vm.count("path")) 
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << _PATH_ << std::endl;}

	if (vm.count("event")) {
    	std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Event #" << vm["event"].as<unsigned long int>() << ".\n";}
    else
    {
        std::cout << "event not set." << std::endl;
        return 0;
    }
    
    viewer(_PATH_, vm["event"].as<unsigned long int>(), THRESHOLD, SIZE_CUT_X, SIZE_CUT_Y, EFF_LOW_CHARGE_CUT_X, EFF_HIGH_CHARGE_CUT_X, EFF_LOW_CHARGE_CUT_Y, EFF_HIGH_CHARGE_CUT_Y);
	return 0;
}
