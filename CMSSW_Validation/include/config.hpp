#include <fstream>

class CONFIG
{
    public:
        CONFIG();
        ~CONFIG();
        void Set(double&, double&, double&, double&);
    private:
        std::ofstream _CONFIG_FILE_;
};