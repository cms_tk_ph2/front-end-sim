#include "load.hpp"
#include <iostream>
#include <TFile.h>

NodeFile::NodeFile(std::string _PATH_, std::string _VERBOSE_LEVEL_)
{
    TFile* _file = TFile::Open((_PATH_ + "/output/data.root").c_str());
    MAIN_FILE = _file;
    const std::string DETECTOR_NAME = "Detector";

    if (_VERBOSE_LEVEL_ == "LOG")
    {
        if (!_file) {std::cout << "\033[1;31m[FATAL]\033[0;37m: There was an issue with opening the \"data.root\" (@ \033[1;34m" << _file << "\033[0;37m) file. Aborting (exit code := 1)." << std::endl; std::exit(1);}
        else {std::cout << "\033[1;32m[SUCCESS]\033[0;37m -- File @ (\033[1;31m" << _PATH_ + "/output/data.root" << "\033[0;37m)" <<  "is ready to be processed." << std::endl;}
    }
    
    TTree* PixelHit_Tree = static_cast<TTree*>(_file->Get("PixelHit"));

    if (_VERBOSE_LEVEL_ == "LOG")
    {
        if (!PixelHit_Tree) {std::cout << "[FATAL]: Could not load \"PixelHit\" TTree. Aborting (exit code := 1)." << std::endl; std::exit(1);}
        else 
        {
            std::cout << "	↳ \033[1;34m[INFO]\033[0;37m ➤➤ \033[1;33mPixelHit\033[0;37m TTree   (@\033[1;33m" << PixelHit_Tree << "\033[0;37m) \033[1;32m✓\033[0;37m" << std::endl;
            PIXEL_HIT_TREE = PixelHit_Tree;
        }
    }

    TBranch* Detector_Branch_PX = PixelHit_Tree->FindBranch(DETECTOR_NAME.c_str());

    if (_VERBOSE_LEVEL_ == "LOG")
    {
        if (!Detector_Branch_PX) {std::cout << "[FATAL]: Could not load the main detector TBranch from the \"PixelHit\" TTree. Aborting (exit code := 1)." << std::endl; std::exit(1);} 
        else 
        {
            std::cout << "        ↳ \033[1;34m[INFO]\033[0;37m ➤➤ Detector TBranch (@\033[1;33m" << Detector_Branch_PX << "\033[0;37m) \033[1;32m✓\033[0;37m" << std::endl;
            PIXEL_HIT_DBRANCH = Detector_Branch_PX;
            PIXEL_HIT_DBRANCH->SetObject(&PixelHits);
            std::pair<const std::string, TTree*> PIXEL_HIT_ELEMENT("PIXELHIT", PIXEL_HIT_TREE);
            TREE_MAP.insert(PIXEL_HIT_ELEMENT);
        }
    }

    TTree* MCParticle_Tree = static_cast<TTree*>(_file->Get("MCParticle"));

    if (_VERBOSE_LEVEL_ == "LOG")
    {
        if (!MCParticle_Tree) {std::cout << "[FATAL]: Could not load the \"MCParticle\" TTree. Aborting (exit code := 1)." << std::endl; std::exit(1);}
        else 
        {
            std::cout << "        ↳ \033[1;34m[INFO]\033[0;37m ➤➤ \033[1;33mMCParticle\033[0;37m TTree (@\033[1;33m" << MCParticle_Tree << "\033[0;37m) \033[1;32m✓\033[0;37m" << std::endl;
            MCPARTICLE_TREE = MCParticle_Tree;
            M_ENTRIES = MCPARTICLE_TREE->GetEntries();
        }
    }

    TBranch* Detector_Branch_MCP = MCParticle_Tree->FindBranch(DETECTOR_NAME.c_str());

    if (_VERBOSE_LEVEL_ == "LOG")
    {
        if (!Detector_Branch_MCP) {std::cout << "[FATAL]: Could not load the main detector TBranch from the \"MCParticle\" TTree. Aborting (exit code := 1)." << std::endl; std::exit(1);}
        else 
        {
            std::cout << "        ↳ \033[1;34m[INFO]\033[0;37m ➤➤ Detector TBranch (@\033[1;33m" << Detector_Branch_MCP << "\033[0;37m) \033[1;32m✓\033[0;37m" << std::endl;
            MCPARTICLE_DBRANCH = Detector_Branch_MCP;
            MCPARTICLE_DBRANCH->SetObject(&MCParticles);
            std::pair<const std::string, TTree*> MCPARTICLE_ELEMENT("MCPARTICLE", MCPARTICLE_TREE);
            TREE_MAP.insert(MCPARTICLE_ELEMENT);
        }
    }    
}
NodeFile::~NodeFile()
{
    delete MAIN_FILE;
}
std::vector<allpix::PixelHit*>& NodeFile::GetPixelHits() {return PixelHits;}
std::vector<allpix::MCParticle*>& NodeFile::GetMCParticles() {return MCParticles;}
void NodeFile::SetEntry(unsigned long int _EVENT_) 
{
    PIXEL_HIT_TREE->GetEntry(_EVENT_);
    MCPARTICLE_TREE->GetEntry(_EVENT_);
}
std::map<const std::string, TTree*> NodeFile::GetMap() {return TREE_MAP;}
unsigned long int NodeFile::GetEvents() {return M_ENTRIES;}
