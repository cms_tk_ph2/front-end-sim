#include "cluster.hpp"
#include <algorithm>
#include <set>
#include <iostream>

Clusters::Clusters(std::vector<allpix::PixelHit*> PixelHits, std::vector<allpix::MCParticle*> MCParticles, bool DEBUG = 0)
{
    _size_ = 0;
    if (DEBUG)
    {
        _DEBUG_ = 1;
        std::cout << "\033[1;34m[DEBUG]\033[0;37m PIXEL HITS IN EVENT: ";
        for (auto& pixhit : PixelHits)
        {
            std::cout << "(" << pixhit->getIndex().x() << ", " << pixhit->getIndex().y() << ") ";
        }
        std::cout << std::endl;
    }

    std::vector<allpix::PixelHit*> COPY(PixelHits);
    if (_DEBUG_)
    {
        std::cout << "\033[1;34m[DEBUG]\033[0;37m PIXEL HITS IN COPY >> ";
        for (auto& pixhit : COPY) {std::cout << "(" << pixhit->getIndex().x() << ", " << pixhit->getIndex().y() << ") ";}
        std::cout << std::endl;
    }

    while (COPY.size() != 0)
    {
        Cluster NEW_CLUSTER;
        if (_DEBUG_)
        {
            std::cout << "\033[1;34m[DEBUG]\033[0;37m EMPTY CLUSTER CREATED AT \033[1;34m@" << &NEW_CLUSTER << "\033[0;37m";
            std::cout << std::endl;
        }
        CompleteCluster(NEW_CLUSTER, COPY, 0);
        ClusterVector.push_back(NEW_CLUSTER);
        NEW_CLUSTER._size_ = NEW_CLUSTER.PixHitList.size();
    }

    if (_DEBUG_)
    {
        std::cout << "\033[1;34m[DEBUG]\033[0;37m CLUSTER NUMBER: " << _size_;
        std::cout << std::endl;
    }

    _size_ = ClusterVector.size();
    for (auto& cluster : ClusterVector)
    {
        /** Setting the Cluster Width and Heigth **/
        std::vector<unsigned long int> X;
        std::vector<unsigned long int> Y;
        std::set<const allpix::MCParticle*> MCParticles;
        for (auto& pixhit : (cluster).PixHitList)
        {
            auto mcps = pixhit->getPrimaryMCParticles();
            for (auto mcp : mcps)
            {
                MCParticles.insert(mcp);
            }
            X.push_back((pixhit)->getIndex().x());
            Y.push_back((pixhit)->getIndex().y());
        }
        unsigned long int WIDTH = *std::max_element(X.begin(), X.end()) - *std::min_element(X.begin(), X.end()) + 1;
        unsigned long int HEIGHT = *std::max_element(Y.begin(), Y.end()) - *std::min_element(Y.begin(), Y.end()) + 1;
	    (cluster)._size_x = WIDTH;
        (cluster)._size_y = HEIGHT;
        /******************************************/
        for (auto& i : MCParticles)
        {
            cluster.MCParticlesList.push_back(i);
        }
        if (MCParticles.size() == 1) {cluster.is_Normal = 1;}
    }
}
Clusters::~Clusters(){}
bool Clusters::AreClose(allpix::PixelHit* PIXEL_1, allpix::PixelHit* PIXEL_2)
{
    const unsigned long int METRIC_CUT = 1;
    auto X1 = (*PIXEL_1).getIndex().x(); auto X2 = (*PIXEL_2).getIndex().x();
    auto Y1 = (*PIXEL_1).getIndex().y(); auto Y2 = (*PIXEL_2).getIndex().y();
    //return (bool)(std::abs((int)(X1 - X2)) + std::abs((int)(Y1 - Y2)) == 1); //Old Clustering.
    return (bool)((std::abs((int)(X1 - Y1 + Y2 - X2)) + std::abs((int)(X1 + Y1 - X2 - Y2))) <= 2 * METRIC_CUT); // New Clustering
}
void Clusters::CompleteCluster(Cluster& _CLUSTER_, std::vector<allpix::PixelHit*>& _COPY_, unsigned long int _FEEDBACK_ = 0)
{
    if (_FEEDBACK_ == 0) 
    {
        allpix::PixelHit* First_PixelHit = _COPY_.front();
        
        if (_DEBUG_)
        {
            std::cout << "\033[1;34m[DEBUG]\033[0;37m Step " << _FEEDBACK_ << ": Adding Pixel (" << First_PixelHit->getIndex().x() << ", " << First_PixelHit->getIndex().y() << ") \033[1;32m[@" <<
                             First_PixelHit << "]\033[0;37m [\033[1;32m" << First_PixelHit << "\033[1;33m >> \033[1;32m" << &_CLUSTER_ << "\033[0;37m]" << std::endl;
        }
        
        _CLUSTER_.Append(First_PixelHit); 
        _COPY_.erase(_COPY_.begin());
        
        if (_DEBUG_)
        {
            std::cout << "\033[1;34m[DEBUG]\033[0;37m Step " << _FEEDBACK_ << ":" << std::endl;
            std::cout << "\033[1;34m[DEBUG]\033[0;37m Step " << _FEEDBACK_ << ": CLUSTER PIXEL HIT LIST >> ";
            for (auto& pixhit : _CLUSTER_.PixHitList) {std::cout << "(" << pixhit->getIndex().x() << ", " << pixhit->getIndex().y() << ") ";}
            std::cout << std::endl;
            std::cout << "\033[1;34m[DEBUG]\033[0;37m Step " << _FEEDBACK_ << ": COPY PIXEL HIT LIST >> ";
            if (_COPY_.size() == 0) {std::cout << "[EMPTY]";}
            for (auto& pixhit : _COPY_) {std::cout << "(" << pixhit->getIndex().x() << ", " << pixhit->getIndex().y() << ") ";}
            std::cout << std::endl;
        }
    }

    while (_COPY_.size() != 0)
    {
        std::set<int> Index_Marker;
        for (long unsigned int i = 0; i < _COPY_.size(); ++i)
        {
            for (std::vector<allpix::PixelHit*>::iterator j = std::next(_CLUSTER_.PixHitList.begin(), _FEEDBACK_); j < _CLUSTER_.PixHitList.end(); ++j)
            {
                if (_DEBUG_)
                {
                    std::string PASS = (AreClose(_COPY_[i], *j)) ? "True" : "False";
                    std::cout << "\033[1;34m[DEBUG]\033[0;37m Step " << _FEEDBACK_ << ": (" << _COPY_[i]->getIndex().x() << ", " << _COPY_[i]->getIndex().y() << ") \033[1;33m~\033[0;37m (" <<
                                    (*j)->getIndex().x() << ", " << (*j)->getIndex().y() << ") := " << PASS << std::endl;
                }
                if (AreClose(_COPY_[i], *j)) {Index_Marker.insert(i);}
            }
        }
        if (Index_Marker.size() == 0) {break;}
        for (auto& index : Index_Marker) 
        {
            if (_DEBUG_)
            {
                std::cout << "\033[1;34m[DEBUG]\033[0;37m Step " << _FEEDBACK_ << ": Adding Pixel (" << _COPY_[index]->getIndex().x() << ", " << _COPY_[index]->getIndex().y() << ") \033[1;32m[@" <<
                             _COPY_[index] << "]\033[0;37m [\033[1;32m" << _COPY_[index] << "\033[1;33m >> \033[1;32m" << &_CLUSTER_ << "\033[0;37m]" << std::endl;
            }
            _CLUSTER_.Append(_COPY_[index]);
        }

        for (std::set<int>::reverse_iterator it = Index_Marker.rbegin(); it != Index_Marker.rend(); ++it) {_COPY_.erase(_COPY_.begin() + (*it));}
        
        if (_DEBUG_)
        {
            std::cout << "\033[1;34m[DEBUG]\033[0;37m Step " << _FEEDBACK_ << ": CLUSTER PIXEL HIT LIST >> ";
            for (auto& pixhit : _CLUSTER_.PixHitList) {std::cout << "(" << pixhit->getIndex().x() << ", " << pixhit->getIndex().y() << ") ";}
            std::cout << std::endl;
            std::cout << "\033[1;34m[DEBUG]\033[0;37m Step " << _FEEDBACK_ << ": COPY PIXEL HIT LIST >> ";
            if (_COPY_.size() == 0) {std::cout << "[EMPTY]";}
            for (auto& pixhit : _COPY_) {std::cout << "(" << pixhit->getIndex().x() << ", " << pixhit->getIndex().y() << ") ";}
            std::cout << std::endl;
        }
        
        _FEEDBACK_++;
        CompleteCluster(_CLUSTER_, _COPY_, _FEEDBACK_);
    }
}
unsigned int Clusters::GetNumber() {return _size_;}
std::vector<Cluster> Clusters::GetClusterVector() {return ClusterVector;}

Cluster::Cluster(){}
Cluster::~Cluster(){}
bool Cluster::IsNormal() {return is_Normal;}
void Cluster::Append(allpix::PixelHit* _NEW_HIT_) {PixHitList.push_back(_NEW_HIT_);}
unsigned long int Cluster::GetWidth() {return _size_x;}
unsigned long int Cluster::GetHeight() {return _size_y;}
std::vector<const allpix::MCParticle*> Cluster::GetMCParticles() {return MCParticlesList;}
std::vector<allpix::PixelHit*> Cluster::GetPixelHitList() {return PixHitList;}
