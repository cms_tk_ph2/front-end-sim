import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

FILE = np.genfromtxt("output.csv", delimiter = ", ")

fig, ax = plt.subplots(nrows = 1, ncols = 1)
ax.plot(FILE[:, 0], FILE[:, 1], ls = '-', color = 'black', label = r"Resolution Curve", lw = 1.5)
mIN = min(FILE[:, 1])
ax.axhline(min(FILE[:, 1]), ls = '--', label = r"Optimal Resolution := {:.2f} $\mu$m".format(mIN))
ax.set_xlabel(r"Shift in Time [ns]", fontsize = 15)
ax.set_ylabel(r"Resolution [$\mu$m]", fontsize = 15)
ax.legend(fontsize = 15)
fig.savefig("ResVsTime.png", dpi = 500, bbox_inches = "tight")
plt.show(block = True)
