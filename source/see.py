import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

FILE = np.genfromtxt("With_Krum_Disp.csv", delimiter = ", ")
FILE1 = np.genfromtxt("Without_Krum_Disp.csv", delimiter = ", ")
FILE2 = np.genfromtxt("With_Krum_Disp_20.csv", delimiter = ", ")

fig, ax = plt.subplots(nrows = 1, ncols = 1)
ax.plot(FILE[:, 0], FILE[:, 1], label = r"10\% Dispersion", color = 'black', ls = '-', alpha = 0.7, lw = 2.0)
ax.plot(FILE2[:, 0], FILE2[:, 1], label = r"30\% Dispersion", color = 'orange', ls = '-', alpha = 0.7, lw = 2.0)
ax.plot(FILE1[:, 0], FILE1[:, 1], label = r"0\% Dispersion", color = 'red', ls = '--', lw = 1)
ax.axhline(min(FILE1[:, 1]), color = 'blue', label = r"Optimal := {:.2f} $\mu$m".format(min(FILE1[:, 1])))
ax.set_xlim([-10, +10])
ax.grid()
ax.legend(fontsize = 15)
fig.savefig("residual_dist_comparison.png", dpi = 500, bbox_inches = "tight")
plt.show(block = True)
