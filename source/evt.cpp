#define SENSOR_THICKNESS 0.150
#define PITCH_X 0.025
#define PITCH_Y 0.100

#include <PixelHit.hpp>
#include <MCParticle.hpp>

#include <cluster.hpp>
#include <load.hpp>
#include <log.hpp>
#include <CROCv1.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TRandom3.h>
#include <TDatabasePDG.h>

/** BOOST HEADERS **/
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/foreach.hpp>

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <utility>
#include <algorithm>

namespace POptions  = boost::program_options;
namespace PTree		= boost::property_tree;

std::string DETECTOR_NAME = "Detector";
TRandom3 RV;
LOGGER _GLOBAL_LOG_;
TDatabasePDG GDatabase;
const std::string STATUS = "INFO";

TH1D* ChargeResolution = new TH1D("cr", "Charge Resolution Histogram (On Optimal Calibration); #Delta Q := Q_{est} - Q_{real} [electrons]; # of Events", 500, -2000, 2000);

std::vector<unsigned long int> ReturnMinima(std::vector<double>& _INPUT_VEC_)
{
        const std::vector<double>::iterator START = _INPUT_VEC_.begin();
        const std::vector<double>::iterator END   = _INPUT_VEC_.end();
        unsigned long int min = std::min_element(START, END) - START;
        std::vector<unsigned long int> Min_Addresses {min};
        for (unsigned long int index = 0; index < _INPUT_VEC_.size(); ++index) if (_INPUT_VEC_[index] == _INPUT_VEC_[min] && index != min) {Min_Addresses.push_back(index);}
        return Min_Addresses;
}

std::vector<unsigned long int> ReturnMaxima(std::vector<double>& _INPUT_VEC_)
{
        const std::vector<double>::iterator START = _INPUT_VEC_.begin();
        const std::vector<double>::iterator END   = _INPUT_VEC_.end();
        unsigned long int max = std::max_element(START, END) - START;
        std::vector<unsigned long int> Max_Addresses {max};
        for (unsigned long int index = 0; index < _INPUT_VEC_.size(); ++index) if (_INPUT_VEC_[index] == _INPUT_VEC_[max] && index != max) {Max_Addresses.push_back(index);}
        return Max_Addresses;
}

std::vector<std::pair<double, double>> GetReconstructedPositions(Clusters& _EVT_CLUSTER_, const double SIZE_CUT_X, const double SIZE_CUT_Y, 
				const double EFF_LOW_CHARGE_CUT_X, const double EFF_HIGH_CHARGE_CUT_X, const double EFF_LOW_CHARGE_CUT_Y, const double EFF_HIGH_CHARGE_CUT_Y,
                CROCv1& Readout, std::map<unsigned short int, std::tuple<double, double, double>>& Ref)
{
    std::vector<std::pair<double, double>> output;
    
    for (auto& i : _EVT_CLUSTER_.GetClusterVector())
    {
        auto PixHits = i.GetPixelHitList();
        auto MCPs = i.GetMCParticles();
	    double _alpha_, _beta_;

        if (MCPs.size())
        {
            _alpha_ = TMath::RadToDeg() * TMath::ATan( (MCPs[0]->getGlobalEndPoint().x() - MCPs[0]->getGlobalStartPoint().x()) / (MCPs[0]->getGlobalEndPoint().z() - MCPs[0]->getGlobalStartPoint().z()) );
            _beta_ = TMath::RadToDeg() * TMath::ATan( (MCPs[0]->getGlobalEndPoint().y() - MCPs[0]->getGlobalStartPoint().y()) / (MCPs[0]->getGlobalEndPoint().z() - MCPs[0]->getGlobalStartPoint().z()) );
        }
        else {continue;}

        double XR;
        double YR;
        {
            std::vector<double> X;
            std::vector<double> Y;
            std::vector<double> Charge;
            std::vector<unsigned short int> ToTs;
            std::vector<double> Charge_Est;

            for (auto& pixhit : PixHits)
            {
                X.push_back(pixhit->getPixel().getLocalCenter().x());
                Y.push_back(pixhit->getPixel().getLocalCenter().y());
                Charge.push_back(pixhit->getSignal());
                ToTs.push_back(Readout.GetToT((double)(pixhit->getSignal())));
            }

            for (size_t i = 0; i < ToTs.size(); ++i)
            {
                double charge_estimate_from_tot = std::get<2>(Ref[ToTs[i]]);
                Charge_Est.push_back(charge_estimate_from_tot);
            }

            for (size_t i = 0; i < Charge.size(); ++i)
            {
                ::ChargeResolution->Fill(Charge[i] - Charge_Est[i]);
            }

	        if (!Charge.size()) {continue;}

            std::vector<unsigned long int> min_indices_x = ::ReturnMinima(X);
            std::vector<unsigned long int> min_indices_y = ::ReturnMinima(Y);
            std::vector<unsigned long int> max_indices_x = ::ReturnMaxima(X);
            std::vector<unsigned long int> max_indices_y = ::ReturnMaxima(Y);

            double XG = (*std::min_element(X.begin(), X.end()) + *std::max_element(X.begin(), X.end())) / 2.;
            double YG = (*std::min_element(Y.begin(), Y.end()) + *std::max_element(Y.begin(), Y.end())) / 2.;

            double charge_row_min = 0;
            double charge_row_max = 0;
            double charge_col_min = 0;
            double charge_col_max = 0;

            unsigned long int size_x = i.GetWidth();
            unsigned long int size_y = i.GetHeight();
            
            for (auto& index : min_indices_x) charge_row_min += Charge_Est[index];
            for (auto& index : max_indices_x) charge_row_max += Charge_Est[index];
            for (auto& index : min_indices_y) charge_col_min += Charge_Est[index];
            for (auto& index : max_indices_y) charge_col_max += Charge_Est[index];

            double multip_term_x = ((charge_row_max - charge_row_min) / (2 * (charge_row_min + charge_row_max)));
            double W_Inner_x = (size_x > 2) ? (size_x - 2) * PITCH_X : 0;
            double default_path_x = TMath::Abs(0.150 * TMath::Tan(TMath::DegToRad() * _alpha_));
	        double w_eff_x = std::abs(std::abs(default_path_x) - W_Inner_x);
            double abs_value_x = ((size_x >= SIZE_CUT_X) || (w_eff_x / PITCH_X < EFF_LOW_CHARGE_CUT_X | w_eff_x / PITCH_X > EFF_HIGH_CHARGE_CUT_X)) ? PITCH_X : w_eff_x;

            double multip_term_y = ((charge_col_max - charge_col_min) / (2 * (charge_col_min + charge_col_max)));
            double W_Inner_y = (size_y > 2) ? (size_y - 2) * PITCH_Y : 0;
            double default_path_y = TMath::Abs(0.150 * TMath::Tan(TMath::DegToRad() * _beta_));
            double w_eff_y = std::abs(std::abs(default_path_y) - W_Inner_y);
            double abs_value_y = ((size_y >= SIZE_CUT_Y) || (w_eff_y / PITCH_Y < EFF_LOW_CHARGE_CUT_Y | w_eff_y / PITCH_Y > EFF_HIGH_CHARGE_CUT_Y)) ? PITCH_Y : w_eff_y;

            XR = XG + multip_term_x * abs_value_x;
            YR = YG + multip_term_y * abs_value_y;
        }

        for (auto& mcp : MCPs)
        {
            double mcp_pos_mid_x = (mcp->getLocalStartPoint().x() + mcp->getLocalEndPoint().x()) * 0.5;
            double mcp_pos_mid_y = (mcp->getLocalStartPoint().y() + mcp->getLocalEndPoint().y()) * 0.5;
            std::pair<double, double> Rec_Pos = std::make_pair<double, double>(XR - mcp_pos_mid_x, YR - mcp_pos_mid_y);
            output.push_back(Rec_Pos);
        }
    }
    return output;
}

void viewer(std::string _PATH_, std::string _GEN_PATH_ , std::string _OUTPUT_NAME_, double _THRESHOLD_ = 1000, double THRESHOLD_DISPERSION = 0, double KRUM = 70, double KRUM_DISPERSION = 0)
{
    /** PREPARING THE OUTPUT FILE **/
    std::ofstream output;
    output.open(_OUTPUT_NAME_.c_str());
    /*******************************/
    
    /** SETTING UP THE ALLPIX OUTPUT FILE **/
    NodeFile Example(_PATH_, "LOG");
    std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
    std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();
    unsigned long int Events = Example.GetEvents();
    /***************************************/

    /** GENERATOR ROOT FILE **/
    TFile* _main = TFile::Open(_GEN_PATH_.c_str());
    TTree* _tree = (TTree*)_main->FindObjectAny("tree");
    ::_GLOBAL_LOG_.SendInfo("Found Generator ROOT File with " + std::to_string(_tree->GetEntries()) + " Events.");

    /***************************************/

    /** SETTING UP BRANCHES && VARIABLES **/
    Float_t ltof;
    Float_t pt;
    Float_t E;
    _tree->SetBranchAddress("ltof", &ltof);
    _tree->SetBranchAddress("pt", &pt);
    _tree->SetBranchAddress("E", &E);
    /***************************************/

    for (double dt = 0; dt < +1; dt += 1)
    {
        ::_GLOBAL_LOG_.SendSuccess("Start Running dt := " + std::to_string(dt) + ".");
        CROCv1 Readout_Chip(_THRESHOLD_, THRESHOLD_DISPERSION, KRUM, KRUM_DISPERSION, dt, {"Latched", "0", "0"}, 0, _GLOBAL_LOG_);
        std::map<unsigned short int, std::tuple<double, double, double>> Ref = Readout_Chip.GetNominalReferenceLadderFull();
        std::vector<double> Res_Y;

        for (unsigned long int _evt_ = 0; _evt_ < Events; _evt_++)
        {
            Example.SetEntry(_evt_); // Sets the PixelHits && MCParticles.
            _tree->GetEntry(_evt_);  // Sets the variables from the principal root file.

            /** APPLYING THRESHOLD TO ALL PIXELHITS **/
            std::vector<allpix::PixelHit*> PRESENT_PIXELHITS;
            for (auto& pixhit : PixelHits) 
            {
                double signal = pixhit->getSignal();
                double timeRef = ltof + dt + Readout_Chip.RisingEdge(signal);
                if (signal > _THRESHOLD_ && timeRef <= 25 && timeRef >= 0) {PRESENT_PIXELHITS.push_back(pixhit);}
            }
            /************************/
            Clusters CLUSTERS(PRESENT_PIXELHITS, MCParticles, 0);
            auto Residuals = GetReconstructedPositions(CLUSTERS, 4, 4, 3, 0, 3, 0, Readout_Chip, Ref);
            for (auto& res : Residuals)
            {
                Res_Y.push_back(res.second * 1e3);
            }          
        }

	    TH1D* ResY = new TH1D("cy", "Residuals on Y; #Delta Y [#mum]; #Delta Y [#mum]", 500, - 3 * PITCH_Y * 1e3, + 3 * PITCH_Y * 1e3);
	    for (auto& i : Res_Y) {ResY->Fill(i);}

        output << dt << ", " << ResY->GetStdDev() << std::endl;
        TCanvas* c1 = new TCanvas("c1"); c1->cd(); ResY->Draw(); c1->Print("example.png");
        TCanvas* c2 = new TCanvas("c2"); c2->cd(); ChargeResolution->Draw(); c2->Print("Charge_Resolution.png");

        delete ResY;
        ::_GLOBAL_LOG_.SendSuccess("Finished Running dt := " + std::to_string(dt) + ".");
    }   
    output.close();
}

int main(int ac, char *av[])
{
    std::string CONFIG_PATH;

    std::string ALLPIX_ROOT_FILE;
    std::string GENERATOR_ROOT_FILE;
    std::string OUTPUT_NAME;

	double THRESHOLD = 1000;
    double THRESHOLD_DISPERSION = 0;
    double KRUM = 70;
    double KRUM_DISPERSION = 0;
    double SIZE_CUT_X = 4;
	double SIZE_CUT_Y = 4;
	double EFF_LOW_CHARGE_CUT_X = 3;
	double EFF_HIGH_CHARGE_CUT_X = 0;
	double EFF_LOW_CHARGE_CUT_Y = 3;
	double EFF_HIGH_CHARGE_CUT_Y = 0;

	POptions::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("c", POptions::value<std::string>(&CONFIG_PATH)->required(), "relative path of the config file");

	POptions::variables_map vm;
	POptions::store(POptions::parse_command_line(ac, av, desc), vm);
	POptions::notify(vm);

	if (vm.count("help")) 
    {
        std::cout << desc << "\n";
        return 1;
	}

    if (vm.count("c"))
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << CONFIG_PATH << std::endl;
    }

    PTree::ptree pt;
	PTree::ini_parser::read_ini(CONFIG_PATH, pt);
    if (pt.count("SIMULATE"))
    {
        std::vector<std::string> EXPECTED_CHILDREN{"THRESHOLD", "THRESHOLD_DISPERSION", "KRUM", "KRUM_DISPERSION", "ALLPIX_ROOT_FILE", 
                    "GENERATOR_ROOT_FILE", "OUTPUT_NAME", "SIZE_CUT_X", "SIZE_CUT_Y", "EFF_LOW_CHARGE_CUT_X", "EFF_HIGH_CHARGE_CUT_X", "EFF_LOW_CHARGE_CUT_Y", "EFF_HIGH_CHARGE_CUT_Y"};
        BOOST_FOREACH(const PTree::ptree::value_type &v, pt.get_child("SIMULATE"))
        {
            /** VARIABLES 
             * @param KEY_NAME  := The name of the variable the user wishes to set.
             * @param VALUE		:= The value assigned to that @param KEY_NAME.
             **/
            std::string KEY_NAME = v.first.data();
            std::string VALUE = v.second.data();
            
            /** Check if the KEY_NAME exists in the expected list of expected key names **/
            std::vector<std::string>::iterator finder = std::find(EXPECTED_CHILDREN.begin(), EXPECTED_CHILDREN.end(), KEY_NAME);
            size_t index = finder - EXPECTED_CHILDREN.begin();
            
            /** For index < 3, the key_name is !mandatory!. Otherwise, not. **/
            if (finder != EXPECTED_CHILDREN.end())
            {
                _GLOBAL_LOG_.SendInfo("Required child \"" + KEY_NAME + "\" was found with a value of \"" + VALUE + "\".");
                if (KEY_NAME == "THRESHOLD") {THRESHOLD = std::stod(VALUE);}
                else if (KEY_NAME == "THRESHOLD_DISPERSION") {THRESHOLD_DISPERSION = std::stod(VALUE);}
                else if (KEY_NAME == "KRUM") {KRUM = std::stod(VALUE);}
                else if (KEY_NAME == "KRUM_DISPERSION") {KRUM_DISPERSION = std::stod(VALUE);}
                else if (KEY_NAME == "SIZE_CUT_X") {SIZE_CUT_X = std::stod(VALUE);}
                else if (KEY_NAME == "SIZE_CUT_Y") {SIZE_CUT_Y = std::stod(VALUE);}
                else if (KEY_NAME == "EFF_LOW_CHARGE_CUT_X") {EFF_LOW_CHARGE_CUT_X = std::stod(VALUE);}
                else if (KEY_NAME == "EFF_HIGH_CHARGE_CUT_X") {EFF_HIGH_CHARGE_CUT_X = std::stod(VALUE);}
                else if (KEY_NAME == "EFF_LOW_CHARGE_CUT_Y") {EFF_LOW_CHARGE_CUT_Y = std::stod(VALUE);}
                else if (KEY_NAME == "EFF_HIGH_CHARGE_CUT_Y") {EFF_HIGH_CHARGE_CUT_Y = std::stod(VALUE);}
                else if (KEY_NAME == "ALLPIX_ROOT_FILE") {ALLPIX_ROOT_FILE = VALUE;}
                else if (KEY_NAME == "GENERATOR_ROOT_FILE") {GENERATOR_ROOT_FILE = VALUE;}
                else if (KEY_NAME == "OUTPUT_NAME") {OUTPUT_NAME = VALUE;}
            }
            else if (finder == EXPECTED_CHILDREN.end())
            {
                _GLOBAL_LOG_.SendFatal("Required child \"" + KEY_NAME + "\" was not found. Aborting (Exit Code = 1)");
                /** If a required key is not set, the application is terminated with exit code 1 **/
                std::exit(1);
            }
        }
    }

    // std::cout << THRESHOLD << std::endl;
    // std::cout << THRESHOLD_DISPERSION << std::endl;
    // std::cout << KRUM << std::endl;
    // std::cout << KRUM_DISPERSION << std::endl;
    // std::cout << ALLPIX_ROOT_FILE << std::endl;
    // std::cout << GENERATOR_ROOT_FILE << std::endl;
    // std::cout << OUTPUT_NAME << std::endl;

    viewer(ALLPIX_ROOT_FILE, GENERATOR_ROOT_FILE, OUTPUT_NAME, THRESHOLD, THRESHOLD_DISPERSION, KRUM, KRUM_DISPERSION);
    return 0;
}