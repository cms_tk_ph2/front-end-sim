#define SENSOR_THICKNESS 0.150
#define PITCH_X 0.025
#define PITCH_Y 0.100

#include <PixelHit.hpp>
#include <MCParticle.hpp>

#include <cluster.hpp>
#include <load.hpp>
#include <log.hpp>
#include <CROCv1.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TH1D.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <TMath.h>
#include <TCanvas.h>
#include <TRandom3.h>
#include <TDatabasePDG.h>

/** BOOST HEADERS **/
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/foreach.hpp>

#include <string>
#include <vector>
#include <fstream>
#include <iostream>
#include <utility>
#include <algorithm>

namespace POptions  = boost::program_options;
namespace PTree		= boost::property_tree;

std::string DETECTOR_NAME = "Detector";
TRandom3 RV;
LOGGER _GLOBAL_LOG_;
TDatabasePDG GDatabase;
const std::string STATUS = "INFO";

void viewer(std::string _PATH_, std::string _GEN_PATH_ , std::string _OUTPUT_NAME_, 
            double _THRESHOLD_ = 1000, double THRESHOLD_DISPERSION = 0, double KRUM = 70, double KRUM_DISPERSION = 0,
            unsigned long int EVENT = 0)
{
    /** PREPARING THE OUTPUT FILE **/
    std::ofstream PIXHITS;
    PIXHITS.open((_OUTPUT_NAME_ + "_pixhits.csv").c_str());

    std::ofstream MCPS;
    MCPS.open((_OUTPUT_NAME_ + "_mcps.csv").c_str());
    /*******************************/
    
    /** SETTING UP THE ALLPIX OUTPUT FILE **/
    NodeFile Example(_PATH_, "LOG");
    std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
    std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();
    unsigned long int Events = Example.GetEvents();
    /***************************************/

    /** GENERATOR ROOT FILE **/
    TFile* _main = TFile::Open(_GEN_PATH_.c_str());
    TTree* _tree = (TTree*)_main->FindObjectAny("tree");
    ::_GLOBAL_LOG_.SendInfo("Found Generator ROOT File with " + std::to_string(_tree->GetEntries()) + " Events.");

    /***************************************/

    /** SETTING UP BRANCHES && VARIABLES **/
    Float_t ltof;
    // Float_t pt;
    Float_t E;
    _tree->SetBranchAddress("ltof", &ltof);
    // _tree->SetBranchAddress("pt", &pt);
    _tree->SetBranchAddress("E", &E);
    /***************************************/

    CROCv1 Readout_Chip(_THRESHOLD_, THRESHOLD_DISPERSION, KRUM, KRUM_DISPERSION, 0, {"Synched", "A", "A"}, 0, ::_GLOBAL_LOG_);
    std::cout << "check1" << std::endl;

    Example.SetEntry(EVENT); // Sets the PixelHits && MCParticles.
    std::cout << "check2" << std::endl;

    _tree->GetEntry(EVENT);  // Sets the variables from the principal root file.

    
    /** APPLYING THRESHOLD TO ALL PIXELHITS **/
    std::vector<allpix::PixelHit*> PRESENT_PIXELHITS;
    for (auto& pixhit : PixelHits) 
    {
        double signal = pixhit->getSignal();
        double timeRef = ltof + Readout_Chip.RisingEdge(signal);
        if (signal > _THRESHOLD_ && timeRef <= 25 && timeRef >= 0) {PRESENT_PIXELHITS.push_back(pixhit);}
    }
    /************************/
    Clusters CLUSTERS(PRESENT_PIXELHITS, MCParticles, 0);

    size_t i = 0;
    for (auto& cluster : CLUSTERS.GetClusterVector())
    {
        i++;
        const auto& pixhits = cluster.GetPixelHitList();
        const auto& mcps = cluster.GetMCParticles();

        for (auto& pixhit : pixhits)
        {
            PIXHITS << i << ", " << pixhit->getPixel().getLocalCenter().x() << ", " << pixhit->getPixel().getLocalCenter().y() << ", " << pixhit->getSignal() << ", " << ltof + Readout_Chip.RisingEdge(pixhit->getSignal()) << ", " << Readout_Chip.GetToT(pixhit->getSignal()) << std::endl;
        }

        for (auto& mcp : mcps)
        {
            MCPS << i << ", " << mcp->getLocalStartPoint().x() << ", " << mcp->getLocalStartPoint().y() << ", " <<  mcp->getLocalEndPoint().x() << ", " << mcp->getLocalEndPoint().y() << ", " << mcp->getParticleID() << ", " << ltof << std::endl;
        }
        
    }

    PIXHITS.close();
    MCPS.close();
}

int main(int ac, char *av[])
{
    std::string CONFIG_PATH;

    std::string ALLPIX_ROOT_FILE;
    std::string GENERATOR_ROOT_FILE;
    std::string OUTPUT_NAME;

	double THRESHOLD = 1000;
    double THRESHOLD_DISPERSION = 0;
    double KRUM = 70;
    double KRUM_DISPERSION = 0;
    unsigned long int EVENT = 0;

	POptions::options_description desc("Allowed options");
	desc.add_options()
    ("help", "produce help message")
    ("c", POptions::value<std::string>(&CONFIG_PATH)->required(), "relative path of the config file");

	POptions::variables_map vm;
	POptions::store(POptions::parse_command_line(ac, av, desc), vm);
	POptions::notify(vm);

	if (vm.count("help")) 
    {
        std::cout << desc << "\n";
        return 1;
	}

    if (vm.count("c"))
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << CONFIG_PATH << std::endl;
    }

    PTree::ptree pt;
	PTree::ini_parser::read_ini(CONFIG_PATH, pt);
    if (pt.count("SIMULATE"))
    {
        std::vector<std::string> EXPECTED_CHILDREN{"THRESHOLD", "THRESHOLD_DISPERSION", "KRUM", "KRUM_DISPERSION", "ALLPIX_ROOT_FILE", 
                    "GENERATOR_ROOT_FILE", "OUTPUT_NAME", "EVENT"};
        
        BOOST_FOREACH(const PTree::ptree::value_type &v, pt.get_child("SIMULATE"))
        {
            /** VARIABLES 
             * @param KEY_NAME  := The name of the variable the user wishes to set.
             * @param VALUE		:= The value assigned to that @param KEY_NAME.
             **/
            std::string KEY_NAME = v.first.data();
            std::string VALUE = v.second.data();
            
            /** Check if the KEY_NAME exists in the expected list of expected key names **/
            std::vector<std::string>::iterator finder = std::find(EXPECTED_CHILDREN.begin(), EXPECTED_CHILDREN.end(), KEY_NAME);
            size_t index = finder - EXPECTED_CHILDREN.begin();
            
            /** For index < 3, the key_name is !mandatory!. Otherwise, not. **/
            if (finder != EXPECTED_CHILDREN.end())
            {
                _GLOBAL_LOG_.SendInfo("Required child \"" + KEY_NAME + "\" was found with a value of \"" + VALUE + "\".");
                if (KEY_NAME == "THRESHOLD") {THRESHOLD = std::stod(VALUE);}
                else if (KEY_NAME == "THRESHOLD_DISPERSION") {THRESHOLD_DISPERSION = std::stod(VALUE);}
                else if (KEY_NAME == "KRUM") {KRUM = std::stod(VALUE);}
                else if (KEY_NAME == "KRUM_DISPERSION") {KRUM_DISPERSION = std::stod(VALUE);}
                else if (KEY_NAME == "ALLPIX_ROOT_FILE") {ALLPIX_ROOT_FILE = VALUE;}
                else if (KEY_NAME == "GENERATOR_ROOT_FILE") {GENERATOR_ROOT_FILE = VALUE;}
                else if (KEY_NAME == "OUTPUT_NAME") {OUTPUT_NAME = VALUE;}
                else if (KEY_NAME == "EVENT") {EVENT = std::stoul(VALUE);}
            }
            else if (finder == EXPECTED_CHILDREN.end())
            {
                _GLOBAL_LOG_.SendFatal("Required child \"" + KEY_NAME + "\" was not found. Aborting (Exit Code = 1)");
                /** If a required key is not set, the application is terminated with exit code 1 **/
                std::exit(1);
            }
        }
    }
    std::cout << EVENT << std::endl;
    viewer(ALLPIX_ROOT_FILE, GENERATOR_ROOT_FILE, OUTPUT_NAME, THRESHOLD, THRESHOLD_DISPERSION, KRUM, KRUM_DISPERSION, EVENT);
    return 0;
}
