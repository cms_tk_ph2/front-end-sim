import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

plt.rcParams['text.latex.preamble'] = [r'\usepackage{amsmath}']

FILE = np.genfromtxt("output.csv", delimiter = ", ")

fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (7,4))

ax.plot(FILE[:, 0], FILE[:,1] / 160901, color = 'blue', ls = '-', lw = 2.0, label = r"Previous BX")
ax.plot(FILE[:, 0], FILE[:,2] / 160901, color = 'orange', ls = '-', lw = 2.0, label = r"Current BX")
ax.plot(FILE[:, 0], FILE[:,3] / 160901, color = 'red', ls = '-', lw = 2.0, label = r"Next BX")

ax.set_ylabel(r"Fraction of PixelHits", fontsize = 15)
ax.set_xlabel(r"$\Delta t_{\text{DAQ}}$ [ns]", fontsize = 15)
ax.set_title(r"Inner Barrel, Layer = 1, Ladder = 4", fontsize = 15)
ax.grid()
ax.set_yticks(np.arange(0, 1.1, step = 0.1))
ax.legend(fontsize = 15, loc = 'lower right')
fig.savefig("output.png", dpi = 200)
plt.show(block = True)
