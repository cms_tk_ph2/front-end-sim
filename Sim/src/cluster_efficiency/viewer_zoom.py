import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

FILE = np.genfromtxt("output.csv", delimiter = ", ")
fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (10,4))
ax.set_title(r"20k MCHits in Inner Barrel, Module 5, Ladder 4, Layer 4", fontsize = 15)

# LEVEL = 0.9
# DT_OVER_90 = np.where(FILE[:, 1] > 0.9, 1, 0)
# DT_OVER_90 = np.where(DT_OVER_90 == 1)[0]
# WIDTH = max(FILE[:, 0][DT_OVER_90]) - min(FILE[:, 0][DT_OVER_90])

ax.plot(FILE[:, 0], FILE[:, 1], color = 'blue')
MAX_EFF = max(FILE[:, 1])
DT_OPT = FILE[:,0][np.argmax(FILE[:, 1])]
ax.axvline(DT_OPT, label = r"Optimal $\varepsilon = $" + " {:.2f}\% @ {:.1f} ns".format(MAX_EFF * 100, DT_OPT), color = 'red', zorder = 1)
ax.scatter(DT_OPT, MAX_EFF)
ax.set_xlabel(r"Global DAQ Shift [ns]", fontsize = 15)
ax.set_ylabel(r"Efficiency", fontsize = 15)
ax.legend(fontsize = 15)
ax.set_ylim([0.95, 1])
ax.set_xlim([-4, 20])
fig.savefig("Eff.png", dpi = 500, bbox_inches='tight')
