import numpy as np
import matplotlib.pyplot as plt

plt.rcParams.update({
   "text.usetex": True,
   "font.family": "serif",
   "font.sans-serif": ["Computer Modern Roman"]})

FILE = np.genfromtxt("output.csv", delimiter = ", ")
fig, ax = plt.subplots(nrows = 1, ncols = 1, figsize = (10,4))
ax.set_title(r"20k MCHits in Inner Barrel, Module 5, Ladder 4, Layer 4", fontsize = 15)

# LEVEL = 0.9
# DT_OVER_90 = np.where(FILE[:, 1] > 0.9, 1, 0)
# DT_OVER_90 = np.where(DT_OVER_90 == 1)[0]
# WIDTH = max(FILE[:, 0][DT_OVER_90]) - min(FILE[:, 0][DT_OVER_90])

ax.plot(FILE[:, 0], FILE[:, 1], color = 'blue')
MAX_EFF = max(FILE[:, 1])
DT_OPT = FILE[:,0][np.argmax(FILE[:, 1])]
ax.axvline(DT_OPT, label = r"Optimal $\varepsilon = $" + " {:.2f}\% @ {:.1f} ns".format(MAX_EFF * 100, DT_OPT), color = 'red', zorder = 1)
ax.scatter(DT_OPT, MAX_EFF)
ax.set_xlabel(r"Global DAQ Shift [ns]", fontsize = 15)
ax.set_ylabel(r"Efficiency", fontsize = 15)
ax.legend(fontsize = 15)
fig.savefig("Eff.png", dpi = 500, bbox_inches='tight')

LEVELS = []
WIDTHS = []

DOMAIN = np.linspace(start = min(FILE[:,0]), stop = max(FILE[:, 0]), num = 10000, endpoint = True)
Y = np.interp(DOMAIN, FILE[:, 0], FILE[:, 1])


for LEVEL in np.linspace(start = 0.97, stop = max(FILE[:, 1]), num = 10000, endpoint = True):
    DT_OVER_90 = np.where(Y >= LEVEL, 1, 0)
    DT_OVER_90 = np.where(DT_OVER_90 == 1)[0]
    WIDTH = (max(DOMAIN[DT_OVER_90]) - min(DOMAIN[DT_OVER_90])) / 6
    LEVELS.append(LEVEL)
    WIDTHS.append(WIDTH)

fig1, ax1 = plt.subplots(nrows = 1, ncols = 1, figsize = (10,4))
ax1.plot(LEVELS, WIDTHS, color = 'blue')
ax1.set_xlabel(r"Efficiency Goal", fontsize = 15)
ax1.set_ylabel(r"Upper Estimate on DAQ Shift Precision [ns]", fontsize = 15)
ax1.axvline(max(FILE[:, 1]), color = 'orange', label = r"Maximum Reachable Efficiency")
ax1.legend(fontsize = 15)
ax1.set_yticks(np.arange(0, max(WIDTHS), 1))
ax1.set_title(r"20k MCHits in Inner Barrel, Module 5, Ladder 4, Layer 4", fontsize = 15)
ax1.grid()
fig1.savefig("Widths.png", dpi = 500, bbox_inches='tight')
plt.show(block = True)
