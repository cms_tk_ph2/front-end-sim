#define SENSOR_THICKNESS 0.150
#define PITCH_X 0.025
#define PITCH_Y 0.100
#define SIZE_CUT 4

#include <PixelHit.hpp>
#include <MCParticle.hpp>
#include <cluster.hpp>
#include <load.hpp>

#include <TFile.h>
#include <TTree.h>
#include <TSystemDirectory.h>
#include <TSystem.h>
#include <iostream>
#include <TMath.h>
#include <string>
#include <vector>
#include <fstream>
#include <boost/program_options.hpp>

namespace po = boost::program_options;
std::string DETECTOR_NAME = "Detector";

void viewer(const std::string _PATH_, unsigned long int _evt_)
{
    NodeFile Example(_PATH_, "LOG");
    std::vector<allpix::PixelHit*>& PixelHits = Example.GetPixelHits();
    std::vector<allpix::MCParticle*>& MCParticles = Example.GetMCParticles();
    std::ofstream pixhits, mcparticles;
    pixhits.open("event.csv"); mcparticles.open("mcps.csv");
    Example.SetEntry(_evt_);
    Clusters __C__(PixelHits, MCParticles, 0);
    for (unsigned long int cluster_number = 0; cluster_number < __C__.GetClusterVector().size(); cluster_number++)
    {
        auto cluster = __C__.GetClusterVector()[cluster_number];
        auto pixhitlist = cluster.GetPixelHitList();
        auto mcplist = cluster.GetMCParticles();
        
	for (auto& pixhit : pixhitlist)
        {
            pixhits << cluster_number + 1 << ", " << pixhit->getPixel().getLocalCenter().x() << ", " << pixhit->getPixel().getLocalCenter().y() << ", " << pixhit->getPixel().getLocalCenter().z() << ", " <<  pixhit->getSignal() << std::endl;
        }
        
	for (auto& mcparticle : mcplist)
        {
            mcparticles << cluster_number + 1 << ", " << mcparticle->getLocalStartPoint().x() << ", " << mcparticle->getLocalStartPoint().y() << ", " << mcparticle->getLocalStartPoint().z() << ", " << mcparticle->getLocalEndPoint().x() << ", " << mcparticle->getLocalEndPoint().y() << ", " << mcparticle->getLocalEndPoint().z() << std::endl;
        }

    }
    pixhits.close();
    mcparticles.close();

}

int main(int ac, char *av[])
{
    std::string _PATH_;
    unsigned long int _EVENT_;
	po::options_description desc("Allowed options");
	desc.add_options()
    	("help", "produce help message")
        ("path", po::value<std::string>(&_PATH_)->required(), "relative path of .root file")
    	("event", po::value<unsigned long int>(&_EVENT_), "set event");

	po::variables_map vm;
	po::store(po::parse_command_line(ac, av, desc), vm);
	po::notify(vm);

	if (vm.count("help")) {
    		std::cout << desc << "\n";
    		return 1;
	}

    if (vm.count("path")) 
    {
        std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Path: " << _PATH_ << std::endl;}

	if (vm.count("event")) {
    	std::cout << "\033[1;32m[SUCCESS]\033[0;37m: Selected Event #" << vm["event"].as<unsigned long int>() << ".\n";}
    else
    {
        std::cout << "event not set." << std::endl;
        return 0;
    }
    
    viewer(_PATH_, vm["event"].as<unsigned long int>());
	return 0;
}
