/** 
 * ! This is work in progress !
 * ? This executable simulates tracks of the CMS Inner Tracker
 * ? for any silicon sensor geometry, and for any part of the Inner Tracker.
 * ? The output is a ROOT File with   
 **/

/** STL HEADERS **/
#include <iostream>
#include <sstream>
#include <fstream>
#include <string>
#include <functional>
#include <bits/stdc++.h>
#include <utility>
#include <memory>
#include <stdexcept>
#include <cstdio>
#include <array>
#include <experimental/filesystem>
namespace fs = std::experimental::filesystem;


/** ROOT HEADERS **/
#include <TFile.h>
#include <TTree.h>
#include <TBranch.h>
#include <TError.h>
#include <TDatabasePDG.h>
#include <TVector3.h>
#include <Math/Point3D.h>
#include <Math/Point3Dfwd.h>
#include <TRandom3.h>

/** BOOST HEADERS **/
#include <boost/program_options.hpp>
#include <boost/property_tree/ptree.hpp>
#include <boost/property_tree/ini_parser.hpp>
#include <boost/foreach.hpp>

/** CUSTOM HEADERS **/
#include <log.hpp>
#include <ConfigSections.cpp>
#include <Config.hpp>

/** Miscellaneous Declarations **/
namespace POptions  = boost::program_options;
namespace PTree		= boost::property_tree;

TDatabasePDG GDatabase;
using CMS_GPoint = ROOT::Math::XYZPoint;
TRandom3 RV;

/** GLOBAL VARIABLE DECLARATIONS **/
LOGGER GLOBAL_LOG;

/** EXECUTABLE OPTIONS **/
std::string CONFIG_FILE_PATH;
std::string VERBOSE_LEVEL;

/** SIMULATE SECTION **/
TFile* MAIN_CMSSW_FILE; 		// Main CMSSW TFile --> Comes from @param ::CMSSW_ROOT_FILE
TTree* MAIN_CMSSW_TTREE;  		// Main TTree inside the TFile --> Comes from @param ::MAIN_TTREE_NAME
TFile* OUTPUT_FILE; 			// Output TFile --> Comes from ::FILE_NAME && ::LOCATIONS
TTree* OUTPUT_TTREE;    		// Output TTree inside OUTPUT_FILE --> Comes from ::FILE_NAME && ::LOCATIONS
unsigned long int Written_Events = 0;

std::string CMSSW_ROOT_FILE;	// Path Name of the CMSSW Root File.
std::string  MAIN_TTREE_NAME = "PixelSimHitNtuple"; // Name of the main TTree in the CMSSW File.
std::string LOCATIONS;			// Configuration of Locations to be simulated
std::string FILE_NAME = "output.root";			// Name of the Output Root File.
std::string ALLPIX_BIN_PATH;	// Location of the Allpix Bin Path
std::string LOG_LEVEL;			// Sets the Log Level of the Run
std::string DIR_NAME;
unsigned long int MAX_EVENTS = 1e5;			

/** ALLPIX SECTION **/
std::string MAIN_CONFIG;
std::string DETECTOR_CONFIG;
std::string MODEL_CONFIG;
std::string MODEL_PATHS;

// Expected Children on the Simulate Section.
std::map<const std::string, std::string*> REF_SIMULATE = {
            {"ALLPIX_BIN_PATH", &ALLPIX_BIN_PATH},
            {"LOG_LEVEL", &LOG_LEVEL},
            {"CMSSW_ROOT_FILE", &CMSSW_ROOT_FILE},
            {"TTREE_NAME", &MAIN_TTREE_NAME},
            {"LOCATIONS", &LOCATIONS},
            {"OUTPUT_NAME", &FILE_NAME},
			{"DIR_NAME", &DIR_NAME}
		};

std::map<const std::string, std::string*> REF_ALLPIX = {
			{"MAIN CONFIG", &MAIN_CONFIG},
			{"DETECTOR CONFIG", &DETECTOR_CONFIG},
			{"MODEL CONFIG", &MODEL_CONFIG},
			{"MODEL PATHS", &MODEL_PATHS}
};

bool Criteria(std::string _Command, std::map<const std::string, int*> REF)
{
    std::function<std::vector<std::size_t>(std::string, std::string)> GetAllOccurences;
    GetAllOccurences = [](std::string str, std::string sub)
    {
        std::vector<std::size_t> positions;
        std::size_t pos = str.find(sub, 0);
    
        while(pos != std::string::npos)
        {
            positions.push_back(pos);
            pos = str.find(sub, pos + 1);
        }
        return positions;
    };
    
    _Command.erase(0, 1);
    _Command.erase(_Command.size() - 1);
    auto _pos = GetAllOccurences(_Command, " && ");
    std::vector<std::string> STR;
    const std::size_t Expected = _pos.size() + 1;
    
    if (_pos.size() == 0) 
    {
        STR.push_back(_Command);
    }
    else
    {
        for (std::size_t index = 0; index < _pos.size(); index++) 
        {
            if (index == _pos.size() - 1) 
            {
                if (index == 0) 
                {
                    std::string str1 = _Command.substr(0, _pos[index]);
                    std::string str2 = _Command.substr(_pos[index] + 4);
                    STR.push_back(str1);
                    STR.push_back(str2);
                }
                else
                {
                    std::string str1 = _Command.substr(_pos[index - 1] + 4, _pos[index] - _pos[index - 1] - 4);
                    std::string str2 = _Command.substr(_pos[index] + 4);
                    STR.push_back(str1);
                    STR.push_back(str2);
                }
            }
            else if (index == 0)
            {
                std::string str1 = _Command.substr(0, _pos[index]);
                STR.push_back(str1);
            }
            else
            {
                std::string str1 = _Command.substr(_pos[index - 1] + 4, _pos[index] - _pos[index - 1] -  4);
                STR.push_back(str1);
            }
        }
    }
    
    std::map<std::string, int> INPUT;
    for (std::size_t index = 0; index < STR.size(); index++)
    {
        std::size_t equal_index = STR[index].find(" == ", 0);
        std::string VAR_NAME = STR[index].substr(0, equal_index);
        int VAR_VALUE = std::stoi(STR[index].substr(equal_index + 4));
        INPUT[VAR_NAME] = VAR_VALUE;
    }
    
    bool result = 1;
    for (auto& i : INPUT)
    {
        bool augment = (*REF[i.first] == i.second);
        // std::cout << "Comparing " << *REF[i.first] << " with " << i.second << " --> " << augment <<  std::endl;
        result &= (*REF[i.first] == i.second);    
    }
    
    return result;
}   

std::string exec(const char* _cmd_command_) 
{
    std::array<char, 128> buffer;
    std::string result;
    std::unique_ptr<FILE, decltype(&pclose)> pipe(popen(_cmd_command_, "r"), pclose);
    if (!pipe) 
	{
        throw std::runtime_error("popen() has failed");
    }
    while (fgets(buffer.data(), buffer.size(), pipe.get()) != nullptr) 
	{
        result += buffer.data();
    }
    return result;
}

void CreateDirectory(const std::string& DIR_NAME_, std::size_t _NUM = 0)
{
	std::function<bool(std::size_t)> CHECK;
	CHECK = [&](std::size_t _NUM)
	{

	};
	
	if ( (!fs::is_directory(DIR_NAME_.c_str()) || !fs::exists(DIR_NAME_.c_str())) && _NUM == 0 ) 
	{
    	fs::create_directory(DIR_NAME_.c_str());
	}
	else if ( (!fs::is_directory(DIR_NAME_.c_str()) || !fs::exists(DIR_NAME_.c_str())) && _NUM != 0 ) 
	{
		_NUM++;
		fs::create_directory(DIR_NAME_.c_str());
	}
}

struct Event
{
	double Eta;
	CMS_GPoint GPosition;
	TVector3 GMomentum;
	TVector3 LDirection;
	double pT;
	double Energy;
	Int_t pdgid;
	double alpha;
	double beta;

	Event(const CMS_GPoint Global_Position, const TVector3 Global_Momentum, const TVector3 Local_Direction, Int_t _pdgid)
	{
		GPosition = Global_Position;
		GMomentum = Global_Momentum; // GeV
		LDirection = Local_Direction;
		TParticlePDG* Particle = GDatabase.GetParticle(_pdgid);
		double Mass = Particle->Mass(); // GeV
		pdgid = _pdgid;
		Energy = TMath::Sqrt(Global_Momentum.Mag2() + Mass * Mass) - Mass;		
		Eta = GPosition.Eta();
		pT = TMath::Sqrt(Global_Momentum.X() * Global_Momentum.X() + Global_Momentum.Y() * Global_Momentum.Y());
		alpha = TMath::ATan((LDirection.X() / LDirection.Z())) * TMath::RadToDeg();
		beta = TMath::ATan((LDirection.Y() / LDirection.Z())) * TMath::RadToDeg();
	}

	void Print()
	{
		std::cout << "GP: " << GPosition << " cm, " << "GM: (" << GMomentum(0) << ", " << GMomentum(1) << ", " << GMomentum(2) << ") GeV, pT: " << pT << " GeV, E: " << Energy << " GeV, PID: " << pdgid << "." << std::endl;
	}
};

void ROOT_CHECKER()
{
	gErrorIgnoreLevel = 5000; // Ignore ROOT Errors Messages.

	/** The following block checks whether the Main ROOT File exists **/
	try 
	{
		bool _OPEN_ = 0;
		MAIN_CMSSW_FILE = TFile::Open(CMSSW_ROOT_FILE.c_str());
		if (MAIN_CMSSW_FILE != nullptr) 
		{
			_OPEN_ = 1;
			std::ostringstream oss; oss << MAIN_CMSSW_FILE;
			std::string _MAIN_CMSSW_FILE_ADDRESS_ = oss.str();
			::GLOBAL_LOG.SendSuccess("The ROOT File \"" + CMSSW_ROOT_FILE + "\" (@" + _RED_ + _MAIN_CMSSW_FILE_ADDRESS_ + _RETURN_ + ") was loaded successfully.");	
		} 
		else {_OPEN_ = 0; throw (_OPEN_);}
	}
	catch (bool _STATUS_) 
	{
		std::string _MESSAGE_ = "The TFile \"" + CMSSW_ROOT_FILE + "\" cannot be accessed " + "(" + _BLUE_ + "_OPEN_" + _RETURN_ + " := " + std::to_string(_STATUS_) + ")";
		::GLOBAL_LOG.SendFatal(_MESSAGE_);
		std::exit(1);
	}

	/** The following block checks whether the Main TTree exists **/
	try 
	{
		bool _OPEN_ = 0;
		MAIN_CMSSW_TTREE = (TTree*)MAIN_CMSSW_FILE->FindObjectAny(MAIN_TTREE_NAME.c_str());
		if (MAIN_CMSSW_TTREE != nullptr) 
		{
			_OPEN_ = 1;
			std::ostringstream oss; oss << MAIN_CMSSW_TTREE;
			std::string _MAIN_CMSSW_TTREE_ADDRESS_ = oss.str();
			std::string _MESSAGE_ = "The Main TTree \"" + MAIN_TTREE_NAME + "\" (@" + (std::string)(_RED_ + _MAIN_CMSSW_TTREE_ADDRESS_ + _RETURN_) + ") was read successfully.";
			::GLOBAL_LOG.SendSuccess(_MESSAGE_);
		} 
		else {_OPEN_ = 0; throw (_OPEN_);}
	}
	catch (bool _STATUS_) 
	{
		std::string _MESSAGE_ = "The TTree \"" + MAIN_TTREE_NAME + "\" cannot be accessed (" + (_BLUE_ + (std::string)("_OPEN_") + _RETURN_) + " := " + std::to_string(_STATUS_) + ")";
		::GLOBAL_LOG.SendFatal(_MESSAGE_);
		std::exit(1);
	}

	/** The following block checks whether the Main TTree has the right TBranches. **/
	try
	{
		bool _IS_ACCEPTED_ = 1;
		std::vector<std::string> BRANCH_NAME_LIST;
		for (auto br : *(MAIN_CMSSW_TTREE->GetListOfBranches())) {BRANCH_NAME_LIST.push_back(br->GetName());}
		
		std::function<bool(std::string, std::vector<std::string>)> IsIn = [&BRANCH_NAME_LIST](std::string _STRING_, std::vector<std::string> _VEC_) 
		{ 
			for (auto& br_name : _VEC_) {if (_STRING_ == br_name) {return 1;}}
			return 0;
		};

		std::vector<unsigned long int> NOT_FOUND;
		std::vector<std::string> BRANCH_NAME_LIST_EXPECTED = {
			"evt",
			"pdgid",
			"eloss",
			"side",
			"disk",
			"module",
			"lx",
			"ly",
			"px",
			"py",
			"pz",
			"ltof", 
			"ltx", 
			"lty", 
			"ltz", 
			"gx", 
			"gy", 
			"gz", 
			"subid", 
			"layer", 
			"ring", 
			"ladder",
			"process",
			"detid"};

		bool count = 1;
		for (unsigned long int index = 0; index < BRANCH_NAME_LIST_EXPECTED.size(); ++index) 
		{
			std::string name = BRANCH_NAME_LIST_EXPECTED[index];
			bool Comparison = IsIn(name, BRANCH_NAME_LIST);
			count = (count && Comparison);
			if (!Comparison) NOT_FOUND.push_back(index);
		}
		if (count == 1) 
		{
			_IS_ACCEPTED_ = 1;
			std::string _MESSAGE_ = "The Main TTree \"" + MAIN_TTREE_NAME + "\" contains all of the expected TBranches (N = " + std::to_string(BRANCH_NAME_LIST_EXPECTED.size()) + ")";
			::GLOBAL_LOG.SendSuccess(_MESSAGE_);
			::GLOBAL_LOG.SendInfo("TBranch Analytics");
			::GLOBAL_LOG.SendInfo("[TBranch Name] [Exists] [Interpretation] [Units]");
			for (auto& br : BRANCH_NAME_LIST_EXPECTED)
			{
				std::string _LOG_INFO_ = br + _GREEN_ + " ✓" + _RETURN_;
				::GLOBAL_LOG.SendInfo(_LOG_INFO_);
			}
		}
		else 
		{
			_IS_ACCEPTED_ = 0;
			auto _STATUS_ = std::tuple<bool, std::vector<std::string>, std::vector<unsigned long int>>(_IS_ACCEPTED_, BRANCH_NAME_LIST_EXPECTED, NOT_FOUND);
			throw (_STATUS_);
		}
	}
	catch (std::tuple<bool, std::vector<std::string>, std::vector<unsigned long int>> _STATUS_) 
	{
		if (std::get<0>(_STATUS_) == 0) 
		{
			std::string not_found_br = "";
			std::vector<unsigned long int> INDEX_VEC = std::get<2>(_STATUS_);
			unsigned long int VEC_SIZE = INDEX_VEC.size();
			for (unsigned long int index = 0; index < VEC_SIZE; ++index) 
			{
				not_found_br += (index != (VEC_SIZE - 1)) ? ("\"" + std::get<1>(_STATUS_)[INDEX_VEC[index]] + "\", ") : ("\"" + std::get<1>(_STATUS_)[INDEX_VEC[index]] + "\".");
			}
			::GLOBAL_LOG.SendWarning("The following TBranches could not be found: " + not_found_br);
		}
	}
}

void ROOT_WRITER()
{
	if (!fs::is_directory(::DIR_NAME.c_str()) || !fs::exists(::DIR_NAME.c_str())) 
	{
    	fs::create_directory(::DIR_NAME.c_str());
	}
	else
	{
		fs::create_directory(::DIR_NAME.c_str());
	}
	std::system(("mkdir " + ::DIR_NAME).c_str());
	::OUTPUT_FILE = new TFile((::DIR_NAME + "/" + ::FILE_NAME).c_str(), "RECREATE", "OUTPUT");
    ::OUTPUT_TTREE = new TTree("tree", "Main");
	::OUTPUT_FILE->cd();
	const unsigned long int Entries = ::MAIN_CMSSW_TTREE->GetEntries();

	/**  
	 * ? Initiating Variables for the Generator ROOT File ?
	 * @param N_EVENT 							:= Number of the Event.
	 * @param N_BX								:= Bunch Crossing ID in CMSSW.
	 * @param N_PDGID							:= PDG ID Code for the particle to be simulated.
	 * @param N_ltx, @param N_lty, @param N_ltz := _x, _y, _z local direction in CMSSW.
	 * @param N_gx, @param N_gy, @param N_gz	:= _x, _y, _z global initial position in AllpixSquared.
	 * @param N_pt 								:= _pt is the transverse momentum in units of GeV.
	 * @param N_ltof							:= _ltof is the local time of flight of the particle.
	 * @param N_E, @param N_eloss				:= _E is the energy of the particle, _eloss is the energy loss inside the sensor.
	 * **/

	int N_EVENT, N_BX, N_PDGID; 
	float N_ltx, N_lty, N_ltz;			
	float N_gx, N_gy, N_gz, N_eta;
	float N_pt;
	float N_ltof;
	float N_E, N_Eloss;

	/** Creating TBranches in the New TTree and binding them with the variables above ... **/

	::OUTPUT_TTREE->Branch("idEvent", &N_EVENT);
	::OUTPUT_TTREE->Branch("pdg", &N_PDGID);
	::OUTPUT_TTREE->Branch("BX", &N_BX);
	::OUTPUT_TTREE->Branch("px", &N_ltx);
	::OUTPUT_TTREE->Branch("py", &N_lty);
	::OUTPUT_TTREE->Branch("pz", &N_ltz);
	::OUTPUT_TTREE->Branch("ltof", &N_ltof);
	::OUTPUT_TTREE->Branch("gx", &N_gx);
	::OUTPUT_TTREE->Branch("gy", &N_gy);
	::OUTPUT_TTREE->Branch("gz", &N_gz);
	::OUTPUT_TTREE->Branch("eta", &N_eta);
	::OUTPUT_TTREE->Branch("pt", &N_pt);
	::OUTPUT_TTREE->Branch("E", &N_E);

	/** Initiating Variables and Connecting them with TBranches of the CMSSW TFile ... **/

	Int_t BX[2];
	Int_t pdgid;
	Float_t ltx, lty, ltz;
	Float_t gx, gy, gz;
	Float_t px, py, pz;
	Float_t ltof;
	Int_t subid, layer, disk, ring, module, ladder, side;

	/** Binding Regional Information of the Inner Tracker **/
	::MAIN_CMSSW_TTREE->SetBranchAddress("subid", &subid);
	::MAIN_CMSSW_TTREE->SetBranchAddress("layer", &layer);
	::MAIN_CMSSW_TTREE->SetBranchAddress("ladder", &ladder);
	::MAIN_CMSSW_TTREE->SetBranchAddress("module", &module);
	::MAIN_CMSSW_TTREE->SetBranchAddress("side", &side);
	::MAIN_CMSSW_TTREE->SetBranchAddress("disk", &disk);
	::MAIN_CMSSW_TTREE->SetBranchAddress("ring", &ring);

	/** Binding Local Event Information **/
	::MAIN_CMSSW_TTREE->SetBranchAddress("evt", &BX);
	::MAIN_CMSSW_TTREE->SetBranchAddress("pdgid", &pdgid);
	::MAIN_CMSSW_TTREE->SetBranchAddress("ltx", &ltx);
	::MAIN_CMSSW_TTREE->SetBranchAddress("lty", &lty);
	::MAIN_CMSSW_TTREE->SetBranchAddress("ltz", &ltz);
	::MAIN_CMSSW_TTREE->SetBranchAddress("px", &px);
	::MAIN_CMSSW_TTREE->SetBranchAddress("py", &py);
	::MAIN_CMSSW_TTREE->SetBranchAddress("pz", &pz);
	::MAIN_CMSSW_TTREE->SetBranchAddress("gx", &gx);
	::MAIN_CMSSW_TTREE->SetBranchAddress("gy", &gy);
	::MAIN_CMSSW_TTREE->SetBranchAddress("gz", &gz);
	::MAIN_CMSSW_TTREE->SetBranchAddress("ltof", &ltof);

	std::map<const std::string, int*> REF = {
		{"subid", &subid}, 
		{"layer", &layer}, 
		{"ladder", &ladder},
		{"module", &module}, 
		{"side", &side},
		{"disk", &disk},
		{"ring", &ring}
	};

	unsigned long int num = 0;
	::MAX_EVENTS = (::MAX_EVENTS >= Entries) ? (Entries) : (MAX_EVENTS);
	for (unsigned long evt = 0; evt < Entries; ++evt)
	{
		::MAIN_CMSSW_TTREE->GetEntry(evt);
		if (!::GDatabase.GetParticle(pdgid)) {continue;}
		bool criteria = Criteria(::LOCATIONS, REF);
		if (criteria && num < MAX_EVENTS)
		{
			N_EVENT = num;
			num++;
			N_BX = BX[1];
			N_PDGID = pdgid;
			N_ltx = (bool)(ladder % 2) ? -ltx : ltx;
			N_lty = lty;
			N_ltz = ltz;
			N_gx = ::RV.Gaus(0, 1);
			N_gy = ::RV.Gaus(0, 1);
			N_gz = (ltz > 0) ? -0.075 : +0.075;
			N_ltof = ltof;
			CMS_GPoint EVT_G_Position(gx, gy, gz);
			TVector3 EVT_G_Momentum(px, py, pz);
			TVector3 EVT_L_Direction(ltx, lty, ltz);
			// printf("\033[1;32m(WRITING EVENT %lld)\033[0;37m\n", num);
			Event EVT(EVT_G_Position, EVT_G_Momentum, EVT_L_Direction, N_PDGID);
			N_E = EVT.Energy * 1000;
			N_eta = EVT.Eta;
			N_pt = EVT.pT;
			// EVT.Print();
			::OUTPUT_TTREE->Fill();
		}
		else if (num >= MAX_EVENTS) {break;}
	}
	::OUTPUT_TTREE->Write();
	::Written_Events = ::OUTPUT_TTREE->GetEntries();
	::GLOBAL_LOG.SendSuccess("Wrote " + std::to_string(::OUTPUT_TTREE->GetEntries()) + " Objects to ./" + ::FILE_NAME);
	::OUTPUT_FILE->Close();
	::MAIN_CMSSW_FILE->Close();
}	

void ALLPIX_SETUP()
{
	std::ifstream MAIN_CONFIG_FILE(::MAIN_CONFIG);
	std::ifstream DET_CONFIG(::DETECTOR_CONFIG);
	std::ofstream MAIN_CONFIG_VALIDATED((::DIR_NAME + "/" + ::MAIN_CONFIG + "v"));
	std::ofstream SETUP_CONFIG_VALIDATED((::DIR_NAME + "/" + ::DETECTOR_CONFIG + "v"));
	std::string line;

	std::vector<bool> Checks = {0, 0};
	while (std::getline(MAIN_CONFIG_FILE, line))
	{
		if (line.find("[Allpix]", 0) != std::string::npos) Checks[0] = 1;
		if (line.find("[DepositionGenerator]", 0) != std::string::npos) {Checks[1] = 1; Checks[0] = 0;}
		if (line.find("[ElectricFieldReader]", 0) != std::string::npos) {Checks[1] = 0;}

		if (Checks[0] == 1 && line.find("number_of_events = ", 0) != std::string::npos) {MAIN_CONFIG_VALIDATED << "number_of_events = " << ::Written_Events << std::endl;}
		else if (Checks[0] == 1 && line.find("detectors_file = ") != std::string::npos) {MAIN_CONFIG_VALIDATED << "detectors_file = " << ::DETECTOR_CONFIG + "v" << std::endl;}
		else if (Checks[0] == 1 && line.find("model_paths = ") != std::string::npos) {MAIN_CONFIG_VALIDATED << "model_paths = " << ::MODEL_PATHS << std::endl;}
		else if (Checks[0] == 1 && line.find("output_directory = ") != std::string::npos) {MAIN_CONFIG_VALIDATED << "output_directory = ./output" << std::endl;}
		else if (Checks[1] == 1 && line.find("file_name = ") != std::string::npos) {MAIN_CONFIG_VALIDATED << "file_name = " << ::FILE_NAME << std::endl;}
		else if (Checks[1] == 1 && line.find("model = ") != std::string::npos) {MAIN_CONFIG_VALIDATED << "model = \"GENIE\"" << std::endl;}
		else {MAIN_CONFIG_VALIDATED << line << std::endl;}
	}

	std::string line1;
	while (std::getline(DET_CONFIG, line1))
	{
		{SETUP_CONFIG_VALIDATED << line1 << std::endl;}
	}

	MAIN_CONFIG_VALIDATED.close();
	MAIN_CONFIG_FILE.close();
	SETUP_CONFIG_VALIDATED.close();
	DET_CONFIG.close();
}

void SIMULATE()
{
	std::system((::ALLPIX_BIN_PATH + "/allpix -c ./" + ::DIR_NAME + "/" + ::MAIN_CONFIG + "v").c_str());
	const std::size_t outcome = std::stoi(::exec("echo $?"));
	if (outcome != 0) {::GLOBAL_LOG.SendFatal("The allpix executable failed to return 0. Aborting (Exit Code := 1)"); std::exit(1);}
}

int main(int ac, char *av[])
{	
	POptions::options_description desc("Allowed Executable Options");
	desc.add_options()
	("help", "Produces Help Message")
	("CONFIG", POptions::value<std::string>(&::CONFIG_FILE_PATH)->required(), "Relative Path of the Main Configuration File")
	("VERBOSE", POptions::value<std::string>(&::VERBOSE_LEVEL)->default_value("WARNING"), "Sets the Log Level {SUCCESS, WARNING, INFO, FATAL}");

	/** Declaration of the Variable Map **/
	POptions::variables_map VAR_MAP;
	try
	{
		/** Boost::program_options parses the ac and av input variables and stores them in @VAR_MAP **/
		POptions::store(POptions::parse_command_line(ac, av, desc), VAR_MAP);
		POptions::notify(VAR_MAP);
		/****/
	}
	catch(const std::exception& e)
	{
		std::cerr << e.what() << '\n';
		std::exit(1);
	}
	
	if(VAR_MAP.count("help"))
	{
		/** Display Help Message Upon Request**/
		std::cout << desc;
		std::exit(1);
	}

	GLOBAL_LOG.SendSuccess("GENERATOR v1.0 Initiated.");

	/** The two options of the executable **/

	PTree::ptree pt;
	try {PTree::ini_parser::read_ini(::CONFIG_FILE_PATH, pt);}
	catch(const std::exception& e)
	{
		GLOBAL_LOG.SendFatal("The File \"" + ::CONFIG_FILE_PATH + "\" could not be read.");
		std::cerr << e.what() << '\n';
		std::exit(1);
	}

	GLOBAL_LOG.SendSuccess("Configuration file \"" + CONFIG_FILE_PATH + "\" is readable.");

	/** Setting the SECTION::SIMULATE **/

	if (pt.count("SIMULATE"))
	{
		const std::string _MESSAGE_ = "The required section " + (std::string)_GREEN_ + "[SIMULATE]" + (std::string)_RETURN_ + " exists.";
		GLOBAL_LOG.SendInfo(_MESSAGE_);

		std::vector<std::string> EXPECTED_CHILDREN{"ALLPIX_BIN_PATH", "CMSSW_ROOT_FILE", "LOCATIONS", "LOG_LEVEL", "TTREE_NAME", "OUTPUT_NAME", "DIR_NAME"};
		BOOST_FOREACH(const PTree::ptree::value_type &v, pt.get_child("SIMULATE"))
		{
			/** VARIABLES 
			 * @param KEY_NAME  := The name of the variable the user wishes to set.
			 * @param VALUE		:= The value assigned to that @param KEY_NAME.
			 **/
			std::string KEY_NAME = v.first.data();
			std::string VALUE = v.second.data();
			
			/** Check if the KEY_NAME exists in the expected list of expected key names **/
			std::vector<std::string>::iterator finder = std::find(EXPECTED_CHILDREN.begin(), EXPECTED_CHILDREN.end(), KEY_NAME);
			size_t index = finder - EXPECTED_CHILDREN.begin();
			
			/** For index < 3, the key_name is !mandatory!. Otherwise, not. **/
			if (index < 7 && finder != EXPECTED_CHILDREN.end())
			{
				GLOBAL_LOG.SendInfo("Required child \"" + KEY_NAME + "\" was found with a value of \"" + VALUE + "\".");
				*::REF_SIMULATE[KEY_NAME] = VALUE;
			}
			else if (index < 3 && finder == EXPECTED_CHILDREN.end())
			{
				GLOBAL_LOG.SendFatal("Required child \"" + KEY_NAME + "\" was not found. Aborting (Exit Code = 1)");
				/** If a required key is not set, the application is terminated with exit code 1 **/
				std::exit(1);
			}
			else if (index > 2 && finder == EXPECTED_CHILDREN.end())
			{
				GLOBAL_LOG.SendInfo("Non-Required child \"" + KEY_NAME + "\" was not found. Setting to default Value.");
			}
			else if (index > 2 && finder != EXPECTED_CHILDREN.end())
			{
				GLOBAL_LOG.SendInfo("Non-Required child \"" + KEY_NAME + "\" was found with a value of \"" + VALUE + "\".");
			}
		}
	}
	else
	{
		GLOBAL_LOG.SendFatal("The section [SIMULATE] was not found in the main configuration file.");
		std::exit(1);
	}

	if (pt.count("ALLPIX"))
	{
		const std::string _MESSAGE_ = "The required section " + (std::string)_GREEN_ + "[ALLPIX]" + (std::string)_RETURN_ + " exists.";
		GLOBAL_LOG.SendInfo(_MESSAGE_);

		std::vector<std::string> EXPECTED_CHILDREN{"MAIN CONFIG", "DETECTOR CONFIG", "MODEL CONFIG", "MODEL PATHS"};
		BOOST_FOREACH(const PTree::ptree::value_type &v, pt.get_child("ALLPIX"))
		{
			/** VARIABLES 
			 * @param KEY_NAME  := The name of the variable the user wishes to set.
			 * @param VALUE		:= The value assigned to that @param KEY_NAME.
			 **/
			std::string KEY_NAME = v.first.data();
			std::string VALUE = v.second.data();
			
			/** Check if the KEY_NAME exists in the expected list of expected key names **/
			std::vector<std::string>::iterator finder = std::find(EXPECTED_CHILDREN.begin(), EXPECTED_CHILDREN.end(), KEY_NAME);
			size_t index = finder - EXPECTED_CHILDREN.begin();
			
			/** For index < 3, the key_name is !mandatory!. Otherwise, not. **/
			if (index < 4 && finder != EXPECTED_CHILDREN.end())
			{
				GLOBAL_LOG.SendInfo("Required child \"" + KEY_NAME + "\" was found with a value of \"" + VALUE + "\".");
				*::REF_ALLPIX[KEY_NAME] = VALUE;
			}
			else if (index < 3 && finder == EXPECTED_CHILDREN.end())
			{
				GLOBAL_LOG.SendFatal("Required child \"" + KEY_NAME + "\" was not found. Aborting (Exit Code = 1)");
				/** If a required key is not set, the application is terminated with exit code 1 **/
				std::exit(1);
			}
			else if (index > 2 && finder == EXPECTED_CHILDREN.end())
			{
				GLOBAL_LOG.SendInfo("Non-Required child \"" + KEY_NAME + "\" was not found. Setting to default Value.");
			}
			else if (index > 2 && finder != EXPECTED_CHILDREN.end())
			{
				GLOBAL_LOG.SendInfo("Non-Required child \"" + KEY_NAME + "\" was found with a value of \"" + VALUE + "\".");
			}
		}
	}
	else
	{
		GLOBAL_LOG.SendFatal("The section [SIMULATE] was not found in the main configuration file.");
		std::exit(1);
	}

	ROOT_CHECKER();
	ROOT_WRITER();
	ALLPIX_SETUP();
	SIMULATE();
	return 0;
}